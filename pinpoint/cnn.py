import warnings

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from cnndataloader import *
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm.auto import trange

# for debugging purposes
debug = False


def debug_print(*args, **kwargs):
    if debug:
        print(*args, **kwargs)


def getDataloaders(
    batch_size=1, test_split=0.2, val_split=0.2, shuffle_dataset=True, random_seed=None
):
    dataset = default_dataset()
    indices = list(range(len(dataset)))
    if shuffle_dataset:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
    test_split = int(np.floor(test_split * len(dataset)))
    val_split = int(np.floor(val_split * len(dataset)))
    train_sampler = SubsetRandomSampler(indices[val_split + test_split :])
    val_sampler = SubsetRandomSampler(indices[0:val_split])
    test_sampler = SubsetRandomSampler(indices[val_split : val_split + test_split])
    dataloaders = dict(
        train=DataLoader(dataset, batch_size=batch_size, sampler=train_sampler),
        val=DataLoader(dataset, batch_size=batch_size, sampler=val_sampler),
        test=DataLoader(dataset, batch_size=batch_size, sampler=test_sampler),
    )
    return dataloaders


def save_state(epoch, save_path=".", **kwargs):
    new_kwargs = {}
    for kw, item in kwargs.items():
        if hasattr(item, "state_dict"):
            item = item.state_dict()
        new_kwargs[kw] = item
    new_kwargs.update(epoch=epoch)
    save_path = Path(save_path) / f"{epoch}.pt"
    torch.save(new_kwargs, str(save_path))


def train_model(
    model,
    dataloaders,
    loss_func,
    num_epochs=100,
    lr=0.1,
    momentum=0.9,
    lr_step_size=100,
    lr_gamma=0.1,
    save_path=".",
):
    # initialize progress bar variables
    pbar = trange(num_epochs)
    pbar.set_description("---")

    # initialize stochastic gradient descent optimizer and step learning rate scheduler
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=momentum)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=lr_step_size, gamma=lr_gamma)
    # initialize accumulators to store train and validation losses
    train_losses = []
    val_losses = []
    # iterate for pbar number of epochs
    epoch = 0
    for epoch in pbar:
        # each epoch consists of 2 phases: (1) training and (2) validation
        for phase in ["train", "val"]:
            if phase == "train":
                model.train()  # (1) training phase
            else:
                model.eval()  # (2) validation phase
            # initialize cumulative variables
            running_loss = 0
            running_size = 0
            # iterate over data
            for datapoint in dataloaders[phase]:
                image_t = datapoint["img"]
                targets_t = datapoint["label"]
                # if in (1) training phase
                if phase == "train":
                    with torch.set_grad_enabled(True):
                        # initialize parameter gradients
                        optimizer.zero_grad()
                        # forward pass and evaluate training loss
                        output_t = model(image_t)
                        loss_t = loss_func(output_t, targets_t)
                        # backward pass and update optimizer
                        loss_t.backward()
                        optimizer.step()
                # else if in (2) validation phase
                else:
                    # forward pass and evaluate validation loss
                    output_t = model(image_t)
                    loss_t = loss_func(output_t, targets_t)
                # track training and validation losses
                assert loss_t >= 0
                running_loss += loss_t.item()
                running_size += 1
            if running_size > 0:
                running_loss /= running_size
            losses = train_losses if phase == "train" else val_losses
            losses.append(running_loss)
        # update learning rate scheduler
        scheduler.step()
        # update the progress bar
        pbar.set_description(
            "Epoch {:03} Train {:.4f} Val {:.4f}".format(
                epoch, train_losses[-1], val_losses[-1]
            )
        )
        if (epoch % 5) == 0:
            save_state(
                epoch,
                model=model,
                save_path=save_path,
                optimizer=optimizer,
                scheduler=scheduler,
            )
        if len(val_losses) > 5:
            loss_change = np.diff(val_losses[-5:]).sum()
            if loss_change > -0.1:
                save_state(
                    epoch,
                    model=model,
                    save_path=save_path,
                    optimizer=optimizer,
                    scheduler=scheduler,
                )
                return np.array(train_losses), np.array(val_losses)

    save_state(epoch, model=model, save_path=save_path)
    return np.array(train_losses), np.array(val_losses)


class BasicPoseTracker(nn.Module):
    def __init__(self, num_keypoints, downsample=1, patch_size=21, num_channels=3):
        super(BasicPoseTracker, self).__init__()
        self.num_keypoints = num_keypoints
        assert downsample >= 1.0, "downsample factor must be at least 1"
        assert patch_size % 2 == 1, "patch size must be odd"
        self.downsample = downsample
        self.patch_size = patch_size
        self.num_channels = num_channels
        self.eff_patch_size = to_odd(patch_size / downsample)

        self.final_conv = nn.Conv2d(
            num_channels,
            num_keypoints,
            self.eff_patch_size,
            padding=(self.eff_patch_size // 2, self.eff_patch_size // 2),
        )

    def forward(self, x):
        """
        Run the "forward pass" of the model to produce logits for
        a batch of images `x`.
        x: (1 x 3 x H x W) "batch" of images, where the batch size
           is always 1.
        returns: (1 x K x H x W) batch of logits for each keypoint.
        """
        original_size = x.shape[-2:]
        original_shape = x.shape[1]
        # 1. Convert to grayscale
        debug_print(f"Original size: {x.shape}")
        if self.num_channels == 1:
            x = x.mean(axis=1)
            x = torch.unsqueeze(x, 1)
            debug_print(f"After mean + unsqueeze: {x.shape}")
        elif self.num_channels != original_shape:
            raise ValueError(
                f"self.num_channels must equal input channels of image. {self.num_channels} != {original_shape}"
            )
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            x = F.interpolate(x, scale_factor=1 / self.downsample)
        # 2. Further downsample the image by the factor specified above.
        debug_print(f"After downsample: {x.shape}")
        # 3. Apply your convolution layer
        x = self.final_conv(x)
        debug_print(f"After conv: {x.shape}")
        # 4. Interpolate back to the original size
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            x = F.interpolate(x, size=original_size)
        debug_print(f"After upsample: {x.shape}")
        return torch.sigmoid(x)


def bernoulli_loss(logits_t, targets_t):
    """Compute the Bernoulli loss as described in the comment above.
    logits_t:  K x P_H x P_W array of logits ($u_{kij}$)
    targets_t: K x P_H x P_W array of binary targets ($y_{kij}$)
    returns: scalar average negative log likelihood.
    """
    # Compute the average negative log likelihood.
    m = torch.maximum(logits_t, torch.zeros_like(logits_t))
    targets_image_size = targets_t.shape[0] * targets_t.shape[1] * targets_t.shape[2]
    L = targets_t * logits_t - m - torch.log(torch.exp(-m) + torch.exp(logits_t - m))
    # Sum over height and width|
    L_per_keypoint = L.sum(axis=2).sum(axis=1)
    # Sum over keypoints
    avg_nll = -1 / targets_image_size * L_per_keypoint.sum()
    return avg_nll


def eval_on_img(data_num, loader="test"):
    global x, y, im
    x = dataloaders[loader].dataset[data_num]["img"]
    im = from_t(x).transpose(1, 2, 0)
    im = im - im.min()
    im /= im.max()
    im = (im * 255).astype("uint8")
    y = from_t(basic_model.forward(x.unsqueeze(0)))[0]
    plt.imshow(cv.cvtColor(im, cv.COLOR_BGR2RGB))
    heatmap = y.max(0)
    plt.imshow(heatmap, alpha=0.5)
    plt.show()


def plot_weights(model):
    wgt: np.ndarray = from_t(model.final_conv.weight)
    wgt = wgt[:, 0, ...]
    for layer in wgt:
        plt.figure()
        plt.imshow(layer)
        plt.show()


if __name__ == "__main__":
    dtype = torch.float32
    plt.ion()
    # get dataloaders
    dataloaders = getDataloaders(
        batch_size=4, test_split=0.2, val_split=0.2, shuffle_dataset=True, random_seed=0
    )

    # Instatiate Model
    #    torch.manual_seed(0)
    basic_model = BasicPoseTracker(4, downsample=1, patch_size=51, num_channels=3)
    basic_model = basic_model.to(device)

    train_losses, val_losses = train_model(
        basic_model,
        dataloaders,
        bernoulli_loss,
        num_epochs=100,
        lr=0.1,
        momentum=0.9,
        lr_step_size=0.1,
        lr_gamma=0.1,
    )

    print()
