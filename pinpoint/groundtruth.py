from __future__ import annotations

import ast
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
import pyqtgraph as pg
from _pinpoint import getPinPoints
from pyqtgraph.Qt import QtWidgets
from qtextras import OptionsDict, ParameterEditor, fns, widgets as w
from s3a import ComplexXYVertices, ComponentIO
from s3a.compio.helpers import registerIoHandler
from s3a.tabledata import TableData

registerIoHandler("sequence", force=True, deserialize=ast.literal_eval)


def appendStem(file: Path, toAppend: str):
    if file is None:
        return toAppend
    return file.with_stem(file.stem + toAppend)


def pinpointTableData():
    td = TableData()
    for field in GroundTruthMarker.pinCol, GroundTruthMarker.flaggedCol:
        td.addField(field)
        td.config["fields"][field.name] = dict(field)
    return td


class GroundTruthMarker(pg.PlotWidget):
    """
    Allows the user to click vertices of an annotation to mark them as pins. Clicking
    an already-marked pin vertex will toggle it into a normal (non-pin) vertex
    """

    pinCol = OptionsDict("Pins", [], "sequence")
    flaggedCol = OptionsDict("Flagged", False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dataItem = pg.PlotDataItem(
            pen="w", clickable=True, symbolPen="#000000", symbolBrush=None
        )
        self.pinPointsScatter = pg.ScatterPlotItem(
            pen="r", brush="r", size=10, clickable=True
        )
        self.infoLabel = QtWidgets.QLabel()
        self.toolsEditor = te = ParameterEditor()
        self.addItem(self.dataItem)
        self.addItem(self.pinPointsScatter)
        self.getViewBox().setAspectLocked()

        self.timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

        self.io = ComponentIO(tableData=pinpointTableData())

        self.pinIdxs = set()
        self.annotationFile = None
        self.contourIdx = -1

        self.dataItem.sigPointsClicked.connect(self.onPointsClicked)
        self.pinPointsScatter.sigClicked.connect(self.onPointsClicked)

        # Shortcut alias
        sc = lambda key: dict(shortcut=key)
        self._nextContourProc = te.registerFunction(
            self.nextContour, runActionTemplate=sc("n")
        )
        te.registerFunction(self.previousContour, runActionTemplate=sc("p"))
        te.registerFunction(self.setAnnotationFile, runActionTemplate=sc("saf"))
        te.registerFunction(self.clearSelectedPoints, runActionTemplate=sc("c"))
        te.registerFunction(
            self.autoSelectPoints,
            docFunc=getPinPoints,
            ignores={"coords", "returnStats", "simplifyStats"},
            runActionTemplate=sc("a"),
        )
        te.registerFunction(
            self.toggleContourFlagged, name="Toggle Flagged", runActionTemplate=sc("f")
        )

        self.saveGtProc = te.registerFunction(self.saveGroundTruth)
        te.registerFunction(self.devConsole)

        vb: pg.ViewBox = self.getViewBox()

        def toggleAspectLocked():
            newLocked = not vb.state["aspectLocked"]
            vb.setAspectLocked(newLocked)

        act = vb.menu.addAction("Toggle Aspect Lock")
        act.triggered.connect(toggleAspectLocked)

    def devConsole(self, *args, **kwargs):
        return w.safeSpawnDevConsole(gt=self)

    def setContour(self, contour: ComplexXYVertices):
        vertices = contour.stack().view(np.ndarray)
        # Make each shape a closed loop
        vertices = np.r_[vertices, vertices[[0]]]
        self.dataItem.setData(
            vertices[:, 0], vertices[:, 1], data=np.arange(vertices.shape[0])
        )
        self.vertices = vertices
        self.pinPointsScatter.clear()
        self.pinIdxs = set()

    def clearSelectedPoints(self):
        self.pinPointsScatter.clear()
        self.pinIdxs = set()

    def autoSelectPoints(self, **kwargs):
        self.clearSelectedPoints()
        # Vertices includes wrap-around index, so don't include this in calculation
        self.updateSelectedIdxs(getPinPoints(self.vertices[:-1], **kwargs))

    def toggleContourFlagged(self):
        idx = self.contourIdx, self.flaggedCol
        self.df.at[idx] = not self.df.at[idx]
        self.refreshLabel()

    def setAnnotationFile(
        self,
        file: str | Path | pd.DataFrame = "",
        addTimestampToSave=False,
        readonly=False,
    ):
        """
        :param file:
        type: filepicker
        """
        if not str(file):
            return
        if not isinstance(file, pd.DataFrame):
            file = Path(file)
            self.df = self.io.importByFileType(file, keepExtraFields=True).reset_index(
                drop=True
            )
            if readonly:
                file = None
        else:
            self.df = self.io.importCsv(file, keepExtraFields=True).reset_index(
                drop=True
            )
            file = None
        self.annotationFile = file

        outColumns = []
        for col in self.df.columns:
            if not isinstance(col, OptionsDict):
                col = OptionsDict(col, "")
            outColumns.append(col)
        self.df.columns = outColumns
        # Ensure pins and flagged columns are there
        mustExist = self.io.tableData.makeComponentDf(len(self.df))
        useCols = set(mustExist.columns).difference(self.df.columns)
        for col in useCols:
            self.df[col] = mustExist[col].to_numpy(type(col.value))
        self.contourIdx = -1
        self.nextContour()
        if addTimestampToSave:
            outFile = appendStem(self.annotationFile, f"_{self.timestamp}")
        else:
            outFile = self.annotationFile
        self.saveGtProc.parameters["file"] = str(outFile)

    def nextContour(self, incr=1):
        self.updateContour(incr)

    def previousContour(self):
        self.updateContour(-self._nextContourProc.parameters["incr"])

    def updateContour(self, contourIncr=1):
        self.saveGroundTruth()
        if self.contourIdx + contourIncr not in self.df.index:
            # Eithoer asked for before beginning or after end
            return
        self.contourIdx += contourIncr
        self.setContour(self.df.at[self.contourIdx, "Vertices"])
        self.updateSelectedIdxs(self.df.at[self.contourIdx, self.pinCol])
        self.getViewBox().autoRange()

    def refreshLabel(self):
        self.infoLabel.setText(
            f"Contour #: {self.contourIdx}\n"
            f"# Pins: {len(self.pinIdxs)}\n"
            f"Flagged: {self.df.at[self.contourIdx, self.flaggedCol]}"
        )

    def onPointsClicked(self, scatter, points=None, ev=None):
        if points is None:
            return
        if scatter is self.dataItem:
            # Shouldn't be able to set multiple points with one click
            points = [points[0]]
        idxs = {p.data() for p in points}
        self.updateSelectedIdxs(idxs)

    def updateSelectedIdxs(self, selectedIdxs: set | list):
        """Take out points already selected, keep all others"""
        removeIdxs = self.pinIdxs.intersection(selectedIdxs)
        self.pinIdxs = self.pinIdxs.union(selectedIdxs).difference(removeIdxs)
        idxs = list(self.pinIdxs)
        vertices = self.vertices[idxs]
        self.pinPointsScatter.setData(vertices[:, 0], vertices[:, 1], data=idxs)
        self.refreshLabel()

    def saveGroundTruth(self, file=None):
        """
        :param file:
        type: filepicker
        """
        if not self.annotationFile or self.contourIdx == -1:
            return
        self.df.at[self.contourIdx, self.pinCol] = sorted(self.pinIdxs)
        if file is None:
            file = self.annotationFile
        self.io.exportByFileType(self.df, file, verifyIntegrity=False, readonly=False)

    def asWindow(self):
        win = w.EasyWidget.buildMainWindow(
            [self, [self.infoLabel, self.toolsEditor]],
            layout="H",
            useSplitter=True,
        )
        return win

    @classmethod
    def readDataset(cls, file: str | Path, returnIo=False):
        io = ComponentIO(tableData=pinpointTableData())
        df = io.importCsv(file)
        if returnIo:
            return df, io
        return df


def main(file="", exec=True, readonly=False):
    if not file:
        file = None
    pg.mkQApp()
    gt = GroundTruthMarker()
    win = gt.asWindow()
    gt.setAnnotationFile(file, readonly=readonly)
    if exec:
        win.show()
        pg.exec()
    return win


def mainCli():
    parser = fns.makeCli(main)
    args = parser.parse_args()
    return main(**vars(args))


if __name__ == "__main__":
    mainCli()
