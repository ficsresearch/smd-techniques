import os
from pathlib import Path

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import torch
from _pinpoint import _getBboxSideIdx
from groundtruth import GroundTruthMarker
from qtextras import fns
from scipy.ndimage import sobel
from skimage.color import label2rgb
from torch.utils.data import Dataset

# Specify that we want our tensors on the GPU and in float32
if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")
dtype = torch.float32

# Helper function to convert between numpy arrays and tensors
to_t = lambda array, dtype_=None: torch.tensor(
    array, device=device, dtype=dtype if np.issubdtype(array.dtype, np.float) else None
)
from_t = lambda tensor: tensor.to("cpu").detach().numpy()
to_odd = lambda x: int(np.ceil(x) // 2 * 2 + 1)


def default_transforms_csv():
    return [
        Normalize(),
        AngleLabels(),
        DilateLabels(),
        MeanAndGradImg(),
        Resize(),
        OneHotLabels(),
        ExtractImgAndLabel(),
        ToTensor(),
    ]


def default_transforms_png():
    return [Normalize(), DilateLabels(), Resize(), ToTensor()]


class Resize(object):
    def __call__(self, datapoint):
        img = cv.resize(datapoint["img"], (512, 512))
        label = cv.resize(
            datapoint["label"], (512, 512), interpolation=cv.INTER_NEAREST
        )
        datapoint.update(img=img, label=label)
        return datapoint


class MeanAndGradImg:
    def __call__(self, datapoint):
        mean = datapoint["img"]
        xx = sobel(mean, axis=1)
        yy = sobel(mean, axis=0)
        datapoint.update(img=np.dstack([mean, xx, yy]))
        return datapoint


class DilateLabels(object):
    def __call__(self, datapoint):
        im_size = datapoint["label"].shape
        kernel_size = int((5.0 / 256) * max(im_size))
        kernel = np.ones((kernel_size, kernel_size), np.uint8)
        label = cv.dilate(datapoint["label"], kernel, iterations=1)
        datapoint.update(label=label)
        return datapoint


class OneHotLabels:
    def __call__(self, datapoint, num_classes=4):
        labels = datapoint["label"]
        new_labels = np.zeros((num_classes, *labels.shape), dtype="float32")
        for ii in range(1, num_classes + 1):
            idx_mask = labels == ii
            new_labels[ii - 1, idx_mask] = 1
        datapoint.update(label=new_labels)
        return datapoint


class ExtractImgAndLabel:
    def __call__(self, datapoint):
        return dict(img=datapoint["img"], label=datapoint["label"])


class AngleLabels:
    def __call__(self, datapoint):
        label = datapoint["label"]
        df_row = datapoint["label_row"]
        poly = datapoint["poly"]
        pin_pts = poly[df_row["Pins"]].astype(int)
        center, wh, box_angle = cv.minAreaRect(poly.astype("single"))
        side_idx = _getBboxSideIdx((poly - center), wh, box_angle)
        side_idx = side_idx[df_row["Pins"]]
        label[pin_pts[:, 1], pin_pts[:, 0]] = side_idx
        datapoint.update(label=label)
        return datapoint


class Normalize:
    def __call__(self, datapoint):
        img = datapoint["img"].astype(float)
        reshaped = img.reshape(-1, 3)
        means = reshaped.mean(0)
        stds = np.std(reshaped, 0)
        img = (img - means[None, None, :]) / stds[None, None, :]
        datapoint.update(img=img, label=datapoint["label"])
        return datapoint


class ToTensor(object):
    def __call__(self, datapoint):
        img = datapoint["img"].transpose((2, 0, 1))
        datapoint.update(img=to_t(img), label=to_t(datapoint["label"]))
        return datapoint


class PinDataset(Dataset):
    def __init__(self, img_path, label_path, transforms=None):
        super(PinDataset, self).__init__()
        if transforms is None:
            transforms = default_transforms_png()
        self.img_files = fns.naturalSorted(
            f"{img_path}/{x}" for x in os.listdir(img_path)
        )
        self.label_files = [f"{label_path}/{x}" for x in os.listdir(label_path)]
        assert len(self.img_files) == len(self.label_files)
        self.transforms = transforms

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, index):
        img = cv.imread(self.img_files[index])
        label = cv.imread(self.label_files[index], cv.IMREAD_GRAYSCALE)
        label[label > 1] = 1
        datapoint = dict(img=img, label=label)
        for transform in self.transforms:
            datapoint = transform(datapoint.copy())
        return datapoint

    def saveDatapoint(self, index):
        datapoint = self[index]
        img = from_t(datapoint["img"]).transpose(1, 2, 0)
        label = from_t(datapoint["label"])
        img = img.astype("uint8")
        label = label.astype("uint8")
        plt.imsave("temp.png", label2rgb(label, img))
        return img, label


class PinCsvDataset(Dataset):
    def __init__(self, img_path, label_csv, transforms=None):
        super().__init__()
        if transforms is None:
            transforms = default_transforms_csv()
        self.img_path = Path(img_path).resolve()
        self.img_files = fns.naturalSorted([x.name for x in self.img_path.glob("*.*")])
        self.label_df = GroundTruthMarker.readDataset(label_csv).reset_index(drop=True)
        self.transforms = transforms

    def __len__(self):
        return len(self.img_files)

    def get_by_name(self, img_name):
        matching_ims = [im == img_name for im in self.img_files]
        if np.count_nonzero(matching_ims) != 1:
            raise ValueError(
                f'Can only use numbers that match exactly one image, "{img_name}" matched {len(matching_ims)}'
            )
        return self[np.argmax(matching_ims)]

    def __getitem__(self, index):
        full_img_path = self.img_path / self.img_files[index]
        img_name = full_img_path.stem
        img = cv.imread(str(full_img_path))
        label_row = self.label_df.iloc[int(img_name)]
        poly = label_row["Vertices"].stack().view(np.ndarray).astype(int)
        poly -= poly.min(0, keepdims=True)
        pin_img = np.zeros(img.shape[:2], dtype=np.uint8)
        pin_pts_xy = poly[label_row["Pins"]].astype(int)
        pin_img[pin_pts_xy[:, 1], pin_pts_xy[:, 0]] = 1
        datapoint = dict(img=img, label_row=label_row, label=pin_img, poly=poly)
        for transform in self.transforms:
            datapoint = transform(datapoint.copy())
        return datapoint

    def saveDatapoint(self, index):
        datapoint = self[index]
        img = from_t(datapoint["img"]).transpose(1, 2, 0)
        label = from_t(datapoint["label"])
        img = img.astype("uint8")
        label = label.astype("uint8")
        plt.imsave("temp.png", label2rgb(label, img))
        return img, label


def default_dataset():
    data_parent = Path(r"C:\Users\Nathan\Dropbox (UFL)\AutoBoM-Pin Counting\ics_jacob")
    img_path = data_parent / "data"
    label_path = data_parent / "ics_jacob.csv"
    return PinCsvDataset(img_path, label_path)


if __name__ == "__main__":
    dataset = default_dataset()
    datapoint = dataset.get_by_name("66.png")
    print()
