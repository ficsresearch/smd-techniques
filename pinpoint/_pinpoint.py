from __future__ import annotations

import math
import signal
from pathlib import Path

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyqtgraph as pg
from pyqtgraph.exporters import SVGExporter
from pyqtgraph.Qt import QtCore, QtGui, QtSvg, QtWidgets
from qtextras import ParameterEditor, widgets as w
from qtextras.typeoverloads import FilePath
from s3a import defaultIo
from scipy import spatial
from scipy.signal import find_peaks, peak_widths


class ItemAwarePlotWidget(pg.PlotWidget):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.myItems = []

    def addMyItem(self, item):
        ret = self.plotItem.addItem(item)
        self.myItems.append(item)
        return ret

    def removeMyItems(self):
        for item in self.myItems:
            self.removeItem(item)


def upsampleVerts(vertices: np.ndarray, nPts: int = 0) -> np.ndarray:
    # Credit: https://stackoverflow.com/a/70664846/9463643
    # Cumulative Euclidean distance between successive polygon points.
    # This will be the "x" for interpolation
    # Ensure shape is connected
    vertices = np.r_[vertices, vertices[[0]]]
    d = np.cumsum(np.r_[0, np.sqrt((np.diff(vertices, axis=0) ** 2).sum(axis=1))])

    # get linearly spaced points along the cumulative Euclidean distance
    if nPts > 0:
        distSampled = np.linspace(0, d.max(), nPts)
    else:
        distSampled = np.arange(0, d.max())

    # interpolate x and y coordinates
    vertsInterped = np.c_[
        np.interp(distSampled, d, vertices[:, 0]),
        np.interp(distSampled, d, vertices[:, 1]),
    ]

    return vertsInterped


def _warpCoords(coords, angle):
    sin, cos = math.sin(angle), math.cos(angle)
    M = np.array([[cos, -sin], [sin, cos]])
    warped = M @ coords.T
    # Coords are 2XM, fix this
    warped = warped.T
    return warped


def unrotate(
    coords,
    returnCenter=False,
    returnAngles=False,
    bboxReductionFactor=2.0,
    missingSideOk=False,
):
    # Align to cardinal "box" so all sides are equally rotated
    center, w_h, initialRotationDeg = cv.minAreaRect(coords.astype("float32"))
    # Turn angles beyond 45 degrees into their negative equivalent
    if abs(initialRotationDeg - 90) < abs(initialRotationDeg):
        initialRotationDeg -= 90
        w_h = w_h[::-1]
    coords = (coords - center).view(np.ndarray)
    # Center is now 0 after normalizing
    warped, angleMembership, cardinalAnglesDeg = _unrotateFromBbox(
        coords, w_h, initialRotationDeg, missingSideOk=missingSideOk
    )
    # After first warp, it is possible for long pins to corrup bbox locations. Use the
    # min y-value of each "unrotated" side to correct box parameters

    # Since cardinal angles are ordered by right, top, left, bottom, they impact width,
    # height, width, height respectively
    trueWh = list(w_h)
    # The middle `sideCoordFraction` data points are considered during bbox reduction
    for ii in range(len(cardinalAnglesDeg)):
        axis = ii % 2
        sideCoords = _getMiddlePct(warped[:, 1], angleMembership == ii)
        if not len(sideCoords):
            # No coordinates available for evaluating the std dev, skip this side
            continue
        trueWh[axis] -= bboxReductionFactor * np.abs(sideCoords).std()
    # "warp" once more now that side membership is just determined by the chip body
    warped, angleMembership, cardinalAnglesDeg = _unrotateFromBbox(
        coords, trueWh, initialRotationDeg, missingSideOk=missingSideOk
    )
    # Make sure all y-coordinates are positive. This affects (impossible?) edge cases
    # where the center of a chip is past the edge of one or more pins
    warped[:, 1] = np.abs(warped[:, 1])
    angles = cardinalAnglesDeg[angleMembership]

    ret = [warped]
    if returnCenter:
        ret.append(center)
    if returnAngles:
        ret.append(angles)
    if len(ret) == 1:
        return ret[0]
    return ret


def _getLongestSegment(mask):
    # Edge case: No samples on a side
    if not np.any(mask):
        return mask
    mask = np.asarray(mask, dtype="uint8")
    nregions, labeled = cv.connectedComponents(mask)
    uniques, counts = np.unique(labeled, return_counts=True)
    # Skip "0" since it represents non-member locations
    counts = counts[uniques > 0]
    uniques = uniques[uniques > 0]

    return (labeled == uniques[counts.argmax()]).ravel()


def _getBboxSideIdx(
    coords, bboxWidthHeight, bboxRotationDeg, center=(0, 0), returnAngles=False
):
    if np.any(center):
        coords = coords - np.array(center).reshape(1, 2)
    simpleBoxPoints = cv.boxPoints((center, bboxWidthHeight, bboxRotationDeg)).astype(
        int
    )
    # Ensure consistent ordering regardless of box orientation from closest to -45 -> end
    boxPtAngles = np.rad2deg(np.arctan2(simpleBoxPoints[:, 1], simpleBoxPoints[:, 0]))
    simpleBoxPoints = np.roll(
        simpleBoxPoints, -np.abs(boxPtAngles + 45).argmin(), axis=0
    )

    binEdges = []
    for pt in simpleBoxPoints:
        edgeAngle = math.degrees(np.arctan2(*pt[::-1]))
        binEdges.append(edgeAngle)
    # Complete the wrapaorund by a positive version of first point
    binEdges.append(binEdges[0] + 360)
    binEdges = np.array(binEdges)
    # Force monotonically increasing wrap-around
    binEdges[binEdges < binEdges[0]] += 360

    # Each side of the box should be unrotated to lie along the 0 degree uniform line
    # This means vertices should align themselves to the rotation angle of the closest
    # bounding box point, which in turn should align itself based on the entire side
    # it's a part of
    intermedRotAnglesDeg = np.rad2deg(np.arctan2(coords[:, 1], coords[:, 0]))
    # Make sure rollover works appropriately for monotonically increasing bin values
    # So the new range of angles will be -45->315 degrees (still covers 360 degrees)
    intermedRotAnglesDeg[(intermedRotAnglesDeg < binEdges[0])] += 360

    # Clip angles to lie on the optimal plane of their box side
    binIdxs = np.digitize(intermedRotAnglesDeg, binEdges)
    # After adding 360 to undervalued angles, it shouldn't be possible to be outside
    # bin edges
    assert binIdxs.min() >= 1, "Bin values below -45 encountered"
    assert binIdxs.max() <= 4, "Bin values above 315 encountered"
    # True rotation determined by where in the binning the angle ended up, since this
    # corresponds to a square edge
    if returnAngles:
        return binIdxs, intermedRotAnglesDeg
    return binIdxs


def _unrotateFromBbox(
    coords, bboxWidthHeight, bboxRotationDeg, center=(0, 0), missingSideOk=False
):
    binIdxs = _getBboxSideIdx(coords, bboxWidthHeight, bboxRotationDeg, center)
    binIdxs -= 1
    # True rotation determined by where in the binning the angle ended up, since this
    # corresponds to a square edge
    finalRotationAnglesDeg = np.array([90, 0, -90, 180]) - bboxRotationDeg
    finalRotationAngles = np.deg2rad(finalRotationAnglesDeg)
    # Since bin edges represent boundaries and not the true rotation angle, index into
    # a new array that *actually* gives the angle to rotate
    warped = np.full_like(coords, np.nan)
    for ii in range(len(finalRotationAngles)):
        membership = binIdxs == ii
        if not np.any(membership):
            if not missingSideOk:
                raise RuntimeError(f"Side {ii} has no points")
            else:
                # No calculation needed since there aren't any points
                continue
        ang = finalRotationAngles[ii]
        warped[membership] = _warpCoords(coords[membership], ang)
    assert np.all(
        np.isfinite(warped)
    ), "Some coordinates did not correspond to a bin edge"

    return warped, binIdxs, finalRotationAnglesDeg


def getPinPoints(
    coords,
    pinHeightPct=0.25,
    pinWidthPct=0.5,
    sideVarThreshold=0.08,
    bboxReductionFactor=2.3,
    returnStats=False,
    simplifyStats=True,
):
    """
    Parameters
    ----------
    coords: ndarray
        Nx2 X-Y coordinates
    pinHeightPct:
        step=0.1
    pinWidthPct:
        limits: [0, 1]
        step: 0.1
    sideVarThreshold:
        step=0.1
    bboxReductionFactor:
        step=0.1
    returnStats:
        ignore=True
    simplifyStats:
        ignore=True
    """
    unsimplified = upsampleVerts(coords).view(np.ndarray)
    unrotated, center, angles = unrotate(
        unsimplified,
        returnCenter=True,
        returnAngles=True,
        bboxReductionFactor=bboxReductionFactor,
    )
    # After unrotating, the y-coordinate is all that matters
    data = unrotated[:, 1].copy()

    # The exact angle doesn't matter here, so turn to for easier grouping
    angles = angles.astype(int)
    # Sides with pins tend to have much higher variance and more uniform value
    # frequency, sides without pins don't. Throw bad sides out early to avoid false
    # positives
    dataSer = pd.Series(data.copy(), angles.copy())
    # Don't favor sides just because of their size

    variances = []
    for idx, angleGrp in dataSer.groupby(dataSer, level=0):
        # Cut off beginning and end of groups for cases where pins get cut off in the
        # bounding box grouping
        boostedAngleGrp = _getMiddlePct(angleGrp, angles == idx).values
        # corrTemplate = boostedAngleGrp - boostedAngleGrp.min()
        # Boost periodic signals through correlation before detecting standard deviation
        # if corrTemplate.ptp() > 0:
        #     boostedAngleGrp = correlate(
        #         corrTemplate, corrTemplate / corrTemplate.ptp(), mode="same"
        #     )
        variances.append(np.var(boostedAngleGrp))
    variancesPct = np.array(variances)
    if any(variancesPct > 0):
        variancesPct /= variancesPct.sum()
    badAngles = np.unique(angles)[variancesPct < sideVarThreshold]
    dataSer.loc[badAngles] = 0

    for idx, angleGrp in dataSer.groupby(angles, level=0):
        angleGrp = _getMiddlePct(angleGrp, angles == idx)
        dataSer.loc[idx] -= angleGrp.min()
        if angleGrp.max() > 0:
            dataSer.loc[idx] /= dataSer.loc[idx].max()

    # Roll until data starts at a contiguous side. Any grouping starting after the
    # first index value should be contiguous Avoid prominence errors by setting a
    # baseline height of 0 at the ends Pins should be at consistent "frequencies" in
    # the data, so bandpass against clearly spurrious peaks
    freq = np.fft.rfft(dataSer)
    normAbsFreq = np.abs(freq)
    suppressedMask = normAbsFreq < np.percentile(normAbsFreq[1:], 80)
    # Disregard DC component during normalization
    suppressedMask[0] = True
    freq[suppressedMask] *= 0.1
    dataSer[:] = np.clip(
        np.real(np.fft.irfft(freq, len(dataSer))) + dataSer.mean(), 0, np.inf
    )
    dataSer /= dataSer.max()
    # Filtering might make some bad angles recover values
    dataSer.loc[badAngles] = 0
    # Preliminarily try finding peaks of any width, and use the common resulting width
    # for the true run
    widthInds, _ = find_peaks(
        dataSer, prominence=np.percentile(dataSer, pinHeightPct), width=1
    )
    widthInfo = peak_widths(dataSer.values, widthInds)[0]
    peakWidth = np.percentile(widthInfo, pinWidthPct) if len(widthInfo) else 1
    inds = findCoordPeaks(
        dataSer,
        width=peakWidth / 2,
        prominence=pinHeightPct,
        distance=peakWidth,
        rel_height=0.5,
    )
    # Account for initial added sample

    # Since "unrotating" cenntered coords around 0, take this into account before indexing
    coords = coords - center
    unsimplified = unsimplified - center
    tree = spatial.KDTree(coords)
    # Make sure found indices are relative to original data points, since currently
    # they're relative to unsimplified vertices.
    _, indsRelToInitialVerts = tree.query(unsimplified[inds])
    if not returnStats:
        return indsRelToInitialVerts
    if simplifyStats:
        inverseTree = spatial.KDTree(unsimplified)
        _, keepIds = inverseTree.query(coords)
        # Force pin indices to exist by "shifting" them to a kept index
        _, inds = inverseTree.query(coords[indsRelToInitialVerts])
    else:
        keepIds = np.ones_like(angles, dtype=bool)

    isPin = np.zeros_like(angles, dtype=bool)
    isPin[inds] = True
    stats = pd.DataFrame()
    for name, arr in zip(
        [
            ["x", "y"],
            ["unrotated_x", "unrotated_y"],
            "angle",
            "is_pin",
            "unrotated_processed_y",
        ],
        [unsimplified, unrotated, angles, isPin, dataSer.to_numpy()],
    ):
        stats[name] = arr[keepIds]
    return stats


def _getMiddlePct(data, indexerMask, pct=0.25):
    """Gets just the middle ``pct`` portion of the data"""
    deconvert = not isinstance(data, pd.Series)
    if deconvert:
        data = pd.Series(data[indexerMask], index=np.flatnonzero(indexerMask))
    # Account for the edge case where a data group is split across the beginning-end of
    # an array
    indexer = np.arange(len(data))
    if len(data) > 2 and indexerMask[0] and indexerMask[-1]:
        # 'argmin' tells us how many samples are rolled over to the start of the
        # indexer array I.e. the first sample from the next angle grouping
        rollSpots = -indexerMask.argmin()
        indexer = np.roll(indexer, rollSpots)
        indexerMask = np.roll(indexerMask, rollSpots)
    data = data.iloc[indexer]
    keeps = _getLongestSegment(indexerMask)
    keepIdxs = np.isin(np.flatnonzero(indexerMask), np.flatnonzero(keeps))
    data = data.loc[keepIdxs]
    n = len(data)
    midpoint = n // 2
    start = max(0, int(midpoint - n * pct))
    # Start can't have negative rollover, but stop can have excess rollover with Python
    # indexing
    indexer = slice(start, midpoint + int(n * pct) + 1)
    data = data.iloc[indexer]
    if deconvert:
        return data.values
    return data


def findCoordPeaks(dataSer, **kwargs):
    """
    Search for peaks in each contiguous labeled block of data.
    :param dataSer: Peak data with indexes corresponding to labels
    :param kwargs: find_peaks kwargs
    :return: indices of peaks in data
    """
    # Make sure rollover from start/end of array doesn't happen
    rollSpots = -np.argmax(dataSer.index != dataSer.index[0])
    data = np.roll(dataSer.values, rollSpots)
    # make sure sides that start right at a peak don't get missed
    data = np.concatenate([[0], data, [0]])
    idxs, _ = find_peaks(data, **kwargs)
    # Account for added front sample
    idxs -= 1
    # Account for rollover
    idxs -= rollSpots
    return idxs % len(dataSer)


def _getContiguousDataGroupings(
    dataSer: pd.Series | pd.DataFrame, sandwichWidth=0, roll=False
):
    """
    Returns each 'block' of data with contiguous, equal labels
    Optionally, each group can be extended by 'sandwichWidth' samples from
    neighboring blocks
    """
    idxs = np.arange(len(dataSer))
    if roll:
        rollSpots = -np.argmax(dataSer.index != dataSer.index[0])
        idxs = np.roll(idxs, rollSpots)
    labels = dataSer.index[idxs]
    dataOffsetSer = dataSer.reset_index(drop=True).loc[idxs]
    # Find the first index of each contiguous block
    firstInds = np.flatnonzero(np.diff(labels) != 0) + 1
    # Start index should always be 0
    firstInds = [0] + firstInds.tolist() + [len(idxs)]
    for ii in range(1, len(firstInds)):
        # First index should always be 0
        start = firstInds[ii - 1] - sandwichWidth
        end = firstInds[ii] + sandwichWidth
        curSlice = np.arange(start, end) % len(idxs)
        yield dataOffsetSer.iloc[curSlice]


class ContourAndPeakWidget(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        p1 = self.contourPlot = ItemAwarePlotWidget()
        p2 = self.peakPlot = ItemAwarePlotWidget()
        self.idLabel = QtWidgets.QLabel()
        self.idLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.tools = ParameterEditor()

        w.EasyWidget.buildMainWindow([self.idLabel, [p1, p2]], layout="V", win=self)
        dw = QtWidgets.QDockWidget(self)
        dw.setWidget(self.tools)
        self.addDockWidget(QtCore.Qt.DockWidgetArea.BottomDockWidgetArea, dw)
        self.tools.show()

        for ch in p1, p2:
            ch.setBackground("w")


def plotContourAndPeakData(stats, show=True, win=None, id_=None):
    if win is None:
        win = ContourAndPeakWidget()
    for plot in win.contourPlot, win.peakPlot:
        plot.removeMyItems()
    opts = dict(pen=pg.mkPen(width=5))

    plotStats(stats, curveOpts=opts, show=False, parent=win.contourPlot)
    plotStats(
        stats,
        x="None",
        y="unrotated_processed_y",
        curveOpts=opts,
        show=False,
        parent=win.peakPlot,
    )
    win.idLabel.setText(f"ID: {id_}")
    if show:
        win.show()
    return win


def plotStats(
    stats,
    x="x",
    y="y",
    color="angle",
    peaks="is_pin",
    show=True,
    curveOpts=None,
    scatterOpts=None,
    colormap="viridis",
    parent=None,
):

    if x not in stats:
        stats = stats.copy()
        stats[x] = stats.index
    groups = stats.groupby(color)
    idxs = list(groups.size().index)
    hues = pg.colormap.get(colormap).getLookupTable(nPts=len(idxs))
    pw = parent if parent else ItemAwarePlotWidget()
    curveOpts = curveOpts or {}
    scatterOpts = scatterOpts or {}
    pen = curveOpts.setdefault("pen", pg.mkPen())
    toPlot = stats[[x, y, color]].set_index(color, drop=False)
    for grp in _getContiguousDataGroupings(toPlot):
        data = grp[[x, y]].to_numpy()
        # All colors should be the same
        curColor = grp.loc[grp.index[0], color]
        pen.setColor(pg.mkColor(hues[idxs.index(curColor)]))
        pw.addMyItem(pg.PlotCurveItem(*data.T, **curveOpts))
    if peaks in stats:
        peaks = stats[peaks]
    else:
        peaks = np.zeros(1, bool)
    if np.any(peaks):
        data = stats.loc[peaks, [x, y]].to_numpy()
        scatterOpts["brush"] = scatterOpts["pen"] = "r"
        pw.addMyItem(pg.ScatterPlotItem(*data.T, **scatterOpts))

    vb: pg.ViewBox = pw.getViewBox()
    texts = [act.text() for act in vb.menu.actions()]
    if "Toggle Aspect Lock" not in texts:

        def toggleAspectLocked():
            newLocked = not vb.state["aspectLocked"]
            vb.setAspectLocked(newLocked)

        act = vb.menu.addAction("Toggle Aspect Lock")
        act.triggered.connect(toggleAspectLocked)

    # if show:
    # pw.show()
    return pw


def scatterColoredCoords(coords, angles, peakIdxs=None):
    fig = plt.figure()
    plt.scatter(*coords.T, c=angles)
    if peakIdxs is not None:
        plt.scatter(*coords[peakIdxs].T, c="r")
    return fig


def getDfPinPoints(annotationFileOrDf, returnStats=False, **kwargs):
    """
    Returns an array of predictions, where each element holds the predicted pin indices
    for each row in the annotation file
    """
    if isinstance(annotationFileOrDf, FilePath.__args__):
        df = defaultIo.importByFileType(annotationFileOrDf)
    else:
        df = annotationFileOrDf
    pins = []
    for complexpoly in df["Vertices"]:
        if len(complexpoly) > 1:
            raise ValueError(
                "PinPoint can only handle single polygons, not groups of polygons.\n"
                f"Expected len(vertices) == 1, got len(vertices) == {len(complexpoly)}"
            )
        verts = complexpoly.stack().view(np.ndarray)
        found = getPinPoints(verts, returnStats=returnStats, **kwargs)
        if not returnStats:
            found = sorted(found)
        pins.append(found)
    return pins


def getErrContours(dataFile, errFile=None):
    df = defaultIo.importByFileType(dataFile, keepExtraFields=True).reset_index(
        drop=True
    )
    df = df[df.Flagged == "False"].reset_index(drop=True)
    if errFile is None:
        errFile = dataFile.parent / (dataFile.stem + "_err.csv")
    errs = pd.read_csv(errFile, index_col=0)
    assert len(df) == len(errs)
    hasErr = (errs["extra"] + errs["missing"]) > 0
    df = df[hasErr]
    return df


def saveContoursToFolder(df, drawPins=False, outFolder="."):
    outFolder = Path(outFolder)
    spi = pg.ScatterPlotItem()
    spi.setPen("r")
    spi.setBrush("r")
    spi.setPxMode(False)
    spi.setSize(4)
    ci = pg.PlotCurveItem()
    ci.setPen("k", width=3)

    if drawPins:
        predicted = getDfPinPoints(df)
    else:
        # Predicted will never be painted, so just leave it blank
        predicted = [[]] * len(df)
    for pins, (idx, row) in zip(predicted, df.iterrows()):
        outName = outFolder / f"{idx}.svg"
        coords = row["Vertices"].stack().view(np.ndarray).astype(float)
        coords = np.r_[coords, coords[[0]]]
        coords -= coords.min(0)
        coords = (coords / coords.max()) * 250
        spi.setData(*coords[pins].T)
        ci.setData(*coords.T)
        generator = QtSvg.QSvgGenerator()
        generator.setFileName(str(outName))
        painter = QtGui.QPainter()
        painter.begin(generator)
        ci.paint(painter, None, None)
        painter.scale(4 / 3, 4 / 3)
        if drawPins:
            spi.paint(painter)
        assert painter.end()


def main(dataFileOrDf, useErrFile=False):
    if not isinstance(dataFileOrDf, pd.DataFrame) and useErrFile:
        df = getErrContours(dataFileOrDf)
    elif not isinstance(dataFileOrDf, pd.DataFrame):
        df = defaultIo.importByFileType(dataFileOrDf, keepExtraFields=True).reset_index(
            drop=True
        )
    else:
        df = dataFileOrDf
    df = df.reset_index(drop=True)
    mainwin = ContourAndPeakWidget()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    ii = 0

    def nextContour(incr=1):
        nonlocal ii
        if (ii + incr) < 0 or (ii + incr) >= len(df.index):
            return
        ii += incr
        id_ = df.index[ii]
        coords_ = df.at[id_, "Vertices"].stack().view(np.ndarray)
        stats = getPinPoints(
            coords_, returnStats=True, simplifyStats=False, pinWidthPct=0.1
        )
        plotContourAndPeakData(stats, win=mainwin, id_=id_)

    def prevContour():
        nextContour(-proc.parameters["incr"])

    exporter = SVGExporter(mainwin.contourPlot.getViewBox())
    exporter.params["background"] = "#fff0"

    def screenshot():
        oldbg = mainwin.contourPlot.backgroundBrush()
        mainwin.contourPlot.setBackground(None)
        exporter.export(copy=True)
        mainwin.contourPlot.setBackground(oldbg)

    proc = mainwin.tools.registerFunction(
        nextContour, runActionTemplate=dict(shortcut="N")
    )
    mainwin.tools.registerFunction(prevContour, runActionTemplate=dict(shortcut="P"))
    mainwin.tools.registerFunction(screenshot, runActionTemplate=dict(shortcut="S"))
    nextContour()
    pg.exec()


if __name__ == "__main__":
    pg.mkQApp()
    # csv_folder = r'C:\Users\Nathan\Desktop\fpic_mockup\FPIC\smd_annotation'
    base = Path(r"C:\Users\Nathan\Desktop\playground\data")
    file = base / "ics_jacob.csv"
    # out = r"C:\Users\Nathan\Desktop\playground\unflagged"
    # df_ = defaultIo.importCsv(file, keepExtraFields=True).reset_index(drop=True)
    # df_ = df_.query('Flagged == "False"')
    # assert len(df_)
    # saveContoursToFolder(df_, outFolder=out)
    main(file)
