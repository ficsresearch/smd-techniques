from __future__ import annotations

from pathlib import Path
import random
import typing as t

import cv2 as cv
import numpy as np
import pandas as pd
import pyqtgraph as pg
from qtextras import (
    OptionsDict,
    ChainedActionGroupParameter,
    ParameterlessInteractor,
    ParameterEditor,
    bindInteractorOptions as bind,
    EasyWidget,
    ImageViewer,
)
from qtpy import QtWidgets, QtCore
from s3a import defaultIo, ComplexXYVertices, TableData, ComponentIO
from s3a.generalutils import getCroppedImage, DirectoryDict, cvImreadRgb
from gemovi.viz.plugins.helpers import (
    TooltipData,
    to_pil_image,
    NumberedItemContainer,
    PixelSizedImageItem,
)


def make_component_io():
    td = TableData()
    for field in PropertyCalculator.get_property_list():
        td.addField(OptionsDict(field, 0.0))
    return ComponentIO(td)


def step_dict(step=0.1, type="float", **kwargs):
    locs = locals()
    locs.update(locs.pop("kwargs"))
    return locs


class PropertyCalculator(ChainedActionGroupParameter):
    def __init__(self, **opts):
        opts.setdefault("name", "Property Calculator")
        super().__init__(**opts)
        interactor = ParameterlessInteractor()
        for stage in self._get_registered_stages():
            self.addStage(stage, interactor=interactor)

    def _get_registered_stages(self):
        return [
            self.format_vertices,
            self.calculate_area_ratio,
            self.calculate_perimeter_ratio,
            self.calculate_aspect_ratio,
        ]

    @staticmethod
    def format_vertices(vertices: ComplexXYVertices | np.ndarray):
        if isinstance(vertices, ComplexXYVertices):
            # Don't handle multiple shapes
            if len(vertices) != 1:
                return dict(vertices=None)
            vertices = vertices[0]
        # Ensure shape is connected
        vertices = np.r_[vertices, vertices[[0]]]
        return dict(vertices=vertices.view(np.ndarray))

    @staticmethod
    def calculate_perimeter_ratio(vertices: ComplexXYVertices | np.ndarray):
        if vertices is None:
            return dict(perimeter=0.0)

        diffs = np.diff(vertices, axis=0)
        point_distances = np.linalg.norm(diffs, axis=1)
        perimeter = np.sum(point_distances)
        # ptp() gives width and height, rect area = 2l + 2w
        rect_perimeter = np.sum(2 * vertices.ptp(axis=0))
        return dict(perimeter=perimeter / rect_perimeter)

    @staticmethod
    def calculate_area_ratio(vertices: ComplexXYVertices | np.ndarray):
        if vertices is None:
            return dict(area=0.0)
        return dict(area=cv.contourArea(vertices) / np.prod(vertices.ptp(axis=0)))

    @staticmethod
    def calculate_aspect_ratio(vertices: ComplexXYVertices | np.ndarray, use_log=True):
        if vertices is None:
            return dict(aspect=0)
        ordered = np.sort(vertices.ptp(0))
        if 0 in ordered:
            return dict(aspect=0)
        aspect = ordered[1] / ordered[0]
        if use_log:
            aspect = np.log10(aspect)

        return dict(aspect=aspect)

    def populate_calculated_properties(self, df: pd.DataFrame):
        out = df.copy()
        entries = []
        for verts in df["Vertices"]:
            entry = self.activate(vertices=verts)
            entry.pop("vertices")
            entries.append(entry)
        new_df = (
            pd.DataFrame(entries)
            .rename(columns=str.title)
            .reset_index(drop=True)
            .dropna()
        )

        out = out[out.columns.difference(new_df.columns)].iloc[new_df.index]
        for kk in reversed(sorted(new_df)):
            out.insert(0, kk, new_df[kk].to_numpy())
        return out

    @classmethod
    def get_property_list(cls):
        dummy = cls(name="dummy")
        dummy_df = defaultIo.tableData.makeComponentDf(1)
        return [
            p
            for p in dummy.populate_calculated_properties(dummy_df)
            if p not in defaultIo.tableData.allFields
        ]


class ImageScatterPlot(pg.ScatterPlotItem):
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.sigHovered.connect(self.on_scatter_hover)

        self.image_plot_group = NumberedItemContainer(PixelSizedImageItem)
        self.image_plot_group.setParentItem(self)
        self.update_image_group()

    def on_scatter_hover(self, item, points, _evt):
        if not len(points):
            item.setToolTip(None)
            return
        tooltip: TooltipData = points[0].data()
        item.setToolTip(tooltip.to_html())

    def scatter_df_data(
        self,
        df: pd.DataFrame,
        x="Area",
        y="Perimeter",
        color="Aspect",
        colormap="viridis",
        update_image_plot=False,
        image_size=128,
    ):
        if image_size is None:
            image_size = TooltipData.default_image_size

        color_values = df[color].to_numpy()
        brush_float = (color_values - color_values.min()) / color_values.ptp()
        brush = pg.colormap.get(colormap).map(brush_float)

        data = [
            TooltipData(
                name=ii,
                label=round(brush_float[ii], 2),
                image_data=verts,
                image_read_function=self.contour_to_image,
                default_image_size=image_size,
            )
            for ii, verts in enumerate(df["Vertices"])
        ]

        self.setData(df[x], df[y], hoverable=True, tip=None, data=data, brush=brush)
        if update_image_plot:
            self.update_image_group()
        else:
            self.image_plot_group.ensure_n_items(0)
        return self

    def update_image_group(
        self,
        image_size: int | tuple[int, int] | None = 35,
        border_thickness=0,
        n_samples=150,
        mask_rect: QtCore.QRectF | None = None,
    ):
        if isinstance(image_size, int):
            image_size = (image_size, image_size)
        spots: list[pg.SpotItem] = self.points()
        if mask_rect is not None:
            spots = spots[self._maskAt(mask_rect)]
        if len(spots) > n_samples:
            spots = random.choices(spots, k=n_samples)
        added = self.image_plot_group.ensure_n_items(len(spots))["added"]
        for item in added:
            item.setPxMode(True)

        for item, spot in zip(self.image_plot_group, spots):
            tooltip: TooltipData = spot.data()
            # False positive on "asarray"
            image = np.asarray(tooltip.evaluate_image_data())  # noqa
            image_xy = spot.pos()
            item.setImage(
                image,
                border=pg.mkPen(color=spot.brush().color(), width=border_thickness),
            )
            if image_size is not None:
                item.setRect(0, 0, *image_size)
            item.setPos(*image_xy)

    @staticmethod
    def contour_to_image(vertices: ComplexXYVertices, canvas_size=512):
        mask = vertices.removeOffset().toMask(fillColor=255)
        max_shape = max(mask.shape)
        resized = cv.resize(
            mask, (0, 0), fx=canvas_size / max_shape, fy=canvas_size / max_shape
        )
        return to_pil_image(resized)


component_io = make_component_io()
default_resize_options = dict(
    shape=(256, 256), rotationDegrees=None, allowTranspose=True
)


class Viewer(QtWidgets.QMainWindow):
    def __init__(self, image_source: str | Path | DirectoryDict = None):
        super().__init__()

        self.plot_widget = pw = pg.PlotWidget()
        pw.setLabel(axis="bottom", text="Area")
        pw.setLabel(axis="left", text="Perimeter")

        self.scatterplot = ImageScatterPlot()
        self.scatterplot.sigClicked.connect(self.on_scatterplot_clicked)
        pw.addItem(self.scatterplot)

        self.preview_image = ImageViewer()
        self.tools = ParameterEditor()

        if not isinstance(image_source, DirectoryDict):
            image_source = DirectoryDict(image_source, cvImreadRgb)
        self.image_source = image_source
        self.subset_df = None
        self.df = None
        self.property_calculator = PropertyCalculator()

        self.tools.registerFunction(self.set_dataframe)
        self.tools.registerFunction(self.set_df_subset)
        self.scatter_image_group_proc = self.tools.registerFunction(
            self.scatterplot.update_image_group
        )
        self.tools.registerFunction(self.save_current_subset)
        self.tools.registerFunction(self.make_image_dataset)

        vb: pg.ViewBox = self.plot_widget.getViewBox()
        vb.sigRangeChanged.connect(self.on_view_range_changed)

        EasyWidget.buildMainWindow(
            [
                pw,
                EasyWidget(
                    [self.tools, self.preview_image], useSplitter=True, layout="V"
                ),
            ],
            layout="H",
            window=self,
            useSplitter=True,
        )

    def on_view_range_changed(self, viewbox, xy_bounds, xy_changed: t.Sequence[bool]):
        bounds_arr = np.array(xy_bounds)
        view_rect = QtCore.QRectF(*bounds_arr[:, 0], *bounds_arr.ptp(axis=1))
        if any(xy_changed):
            self.scatter_image_group_proc.extra["mask_rect"] = view_rect

    def show_subset_sample_index(
        self,
        index: int,
    ):
        if self.subset_df is None:
            return
        sample = self.subset_df.iloc[index]
        image = self.image_source.get(sample["Image File"])
        if image is None:
            return
        image_data = getCroppedImage(image, sample["Vertices"], returnBoundingBox=False)
        self.preview_image.setImage(image_data)
        return image_data

    def on_scatterplot_clicked(self, item, points, _event):
        if not len(points):
            return
        pt = points[0]
        data: TooltipData = pt.data()
        return self.show_subset_sample_index(data.name)

    @bind(
        quantile=step_dict(0.01),
        aspect_cutoff=step_dict(),
        perimeter_cutoff=step_dict(),
        area_cutoff=step_dict(),
    )
    def set_df_subset(
        self, quantile=0.35, aspect_cutoff=0.5, perimeter_cutoff=1, area_cutoff=0.6
    ):
        df = self.df
        # Desire high perimeter + low area = lots of pins, hopefully
        # Note that too low of an area denotes "stringy" components that should be filtered
        # out
        # Filter for square-like aspect ratios (0 = aspect ratio of 1 due to log scale)
        # Square components should be excluded by default, since there are too many false
        # positives
        score = df.Perimeter / df.Area
        valid_mask = (
            (df.Aspect < aspect_cutoff)
            & (df.Perimeter > perimeter_cutoff)
            & (df.Area > area_cutoff)
        )
        score = score[valid_mask]
        df = df[valid_mask]

        subset = df[score >= score.quantile(quantile)]
        self.subset_df = subset.reset_index(drop=True)
        self.scatterplot.scatter_df_data(subset)
        return subset

    @bind(df=dict(title="Dataframe", type="file", value=""))
    def set_dataframe(self, df: pd.DataFrame | str | Path, **subset_kwargs):
        if isinstance(df, (str, Path)) and not Path(df).is_file():
            return
        df = component_io.importCsv(df)
        if any(f not in df for f in component_io.tableData.allFields):
            df = self.property_calculator.populate_calculated_properties(df)
        for prop in self.property_calculator.get_property_list():
            df[prop] = df[prop].astype(float)
        self.df = df
        self.subset_df = self.set_df_subset(**subset_kwargs)

    @bind(file=dict(type="file", value="", nameFilter="*.csv;;"))
    def save_current_subset(self, file: str | Path):
        if not file:
            return
        return component_io.exportCsv(self.subset_df, file)

    @bind(output_folder=dict(type="file", fileMode="Directory"))
    def make_image_dataset(self, output_folder="./pinned_images", **kwargs):
        # Mangle instance id so save names are correct
        to_export = self.subset_df.reset_index(drop=True)
        to_export["Instance ID"] = to_export.index
        image_df = component_io.exportCompImgsDf(
            to_export,
            resizeOptions=default_resize_options,
            source=self.image_source,
            **kwargs,
        ).loc[to_export.index]
        xy_spans = np.row_stack([v.stack().ptp(0) for v in to_export.Vertices])
        needs_rotate = xy_spans[:, 0] > xy_spans[:, 1]
        shape = default_resize_options.get("shape", None)
        if not np.isscalar(shape) and shape[0] != shape[1]:
            # Shape must be the same width and height, otherwise arbitrarily transposing
            # some elements will cause shape mismatches
            needs_rotate[:] = False
        for idx, row in image_df[needs_rotate].iterrows():
            for kk in "image", "labelMask":
                image_df.iat[idx, image_df.columns.get_loc(kk)] = cv.rotate(
                    row[kk], cv.ROTATE_90_CLOCKWISE
                )
        # Allow accessing later during debug as if export was performed directly with
        # zip exporter
        component_io.exportCompImgsZip.exportObject = image_df
        component_io.exportCompImgsZip.writeFile(output_folder, image_df)
        return image_df


def main(annotation_file_or_df, **kwargs):
    pg.mkQApp()
    window = Viewer("C:/Users/ntjess/Desktop/git/fpic-dataset/data/pcb_image")
    window.set_dataframe(annotation_file_or_df, **kwargs)
    window.show()
    return window
