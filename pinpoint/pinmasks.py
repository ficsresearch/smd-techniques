from __future__ import annotations
from pathlib import Path

import cv2 as cv
import fire
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from s3a import ComplexXYVertices, defaultIo, PRJ_ENUMS
from skimage import io

from s3a.generalutils import cvImsaveRgb, subImageFromVertices, cvImreadRgb
from ._pinpoint import getPinPoints


def remove_package_from_mask(
    vertices: ComplexXYVertices,
    resize_mask=False,
    fill_color=255,
    threshold_percent=20,
    return_stats=False,
    return_plot=False,
):
    stats = getPinPoints(vertices.stack(), returnStats=True, simplifyStats=False)
    stats[["x", "y"]] = stats[["x", "y"]].astype("int32")
    box_points = []
    for angle, subdf in stats.groupby("angle"):
        data = subdf["unrotated_y"].values
        if subdf["is_pin"].sum() < 2:
            # Consider every coordinate to belong to the package
            threshold = 100
        else:
            threshold = threshold_percent
        pkg_coords = subdf.loc[
            data <= np.percentile(data, threshold), ["x", "y"]
        ].values
        box_points.append(pkg_coords)
    box_points = np.row_stack(box_points)
    # Turn into a box so poly filling has no strange jagged edges between sides
    # Add a few pixels along the way to prevent off-by-one rounding errors
    center, (width, height), angle = cv.minAreaRect(box_points)
    w_h = (width + 2, height + 2)
    outer_box_points = cv.boxPoints((center, w_h, angle)).astype("int32")
    # Recreate verties since origin was mutated
    vertices = ComplexXYVertices([stats[["x", "y"]].values])
    vertices, offset = vertices.removeOffset(returnOffset=True)
    outer_box_points -= offset
    mask = vertices.toMask(fillColor=fill_color).astype("uint8")
    cv.fillPoly(mask, [outer_box_points], 0)
    if resize_mask:
        mask = resize_pin_mask_from_vertices(mask, vertices)
    base_return = [mask]
    if return_stats:
        base_return.append(stats)
    if return_plot:
        fig = plt.figure()
        fig.gca().axis("equal")
        scatter_box_points = box_points - offset
        plot_outer_box_points = np.row_stack([outer_box_points, outer_box_points[0]])
        plt.plot(*vertices.stack().view(np.ndarray).T, "k")
        plt.scatter(*scatter_box_points.T, c="b")
        plt.plot(*plot_outer_box_points.T, "r")
        return *base_return, fig
    if len(base_return) == 1:
        return base_return[0]
    return tuple(base_return)


default_resize_options = dict(
    rotationDegrees=PRJ_ENUMS.ROTATION_OPTIMAL,
    allowTranspose=True,
    shape=(512, 512),
)


def resize_pin_mask_from_vertices(
    image: np.ndarray | str | Path,
    vertices: ComplexXYVertices = None,
    resize_options: dict = None,
):
    if resize_options is None:
        resize_options = default_resize_options
    if not isinstance(image, np.ndarray):
        image = cvImreadRgb(image, cv.IMREAD_GRAYSCALE)
    if vertices is None:
        vertices = ComplexXYVertices.fromBinaryMask(image)
    sub_image = subImageFromVertices(image, vertices, **resize_options)
    if not sub_image.any():
        return sub_image
    # Optionally cw rotate image if it is not vertically oriented
    hw_ptp = np.ptp(vertices.stack(), axis=0)
    if hw_ptp[0] > hw_ptp[1]:
        sub_image = cv.rotate(sub_image, cv.ROTATE_90_CLOCKWISE)
    return sub_image


def remove_package_from_annotations(
    annotation_file_or_df: str | Path | pd.DataFrame,
    df_name=None,
    skip_if_few_pins=4,
    **kwargs,
) -> dict[str, np.ndarray]:
    if not isinstance(annotation_file_or_df, pd.DataFrame):
        annotation_file = Path(annotation_file_or_df)
        annotation_file_or_df = defaultIo.importCsv(annotation_file_or_df)
    else:
        annotation_file = Path(df_name or "<unnamed>")
    masks = {}
    for ii, vertices in annotation_file_or_df["Vertices"].items():
        mask, stats, *_ = remove_package_from_mask(
            vertices,
            return_stats=True,
            return_plot=False,
            **kwargs,
        )
        if stats["is_pin"].sum() < skip_if_few_pins:
            continue
        masks[annotation_file.stem + f"_id_{ii}.png"] = mask

    return masks


def main(annotation_folder, output_folder=None, skip_if_few_pins=4, **kwargs):
    files = sorted(Path(annotation_folder).glob("*.csv"))
    if output_folder is not None:
        output_folder = Path(output_folder)
    masks = {}
    for annotation_file in files:
        current_masks = remove_package_from_annotations(
            annotation_file, skip_if_few_pins=skip_if_few_pins, **kwargs
        )
        if output_folder is not None:
            for mask_file, mask in current_masks.items():
                cvImsaveRgb(output_folder / mask_file, mask)
        else:
            masks.update(current_masks)
    if output_folder is None:
        return masks


if __name__ == "__main__":
    fire.Fire(main)
    plt.show(block=True)
