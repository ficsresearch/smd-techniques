from __future__ import annotations

import warnings
from collections import defaultdict
from pathlib import Path

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyqtgraph as pg
from _pinpoint import getDfPinPoints, unrotate
from groundtruth import GroundTruthMarker
from scipy.spatial import KDTree


def normedCoordError(vertices, predictedPinIdxs, truePinIdxs):
    """
    Calculates the normalized coordinate error between the predicted and true
    pin locations. Complications arise when not all pins or too many pins are found.
    A KD-Tree is used to find the closest pin for each predicted pin, where the distance
    threshold determines a match is not possible
    """
    # Normalize distance to pin size so errors are all between 0 and 1
    center, *_rest = cv.minAreaRect(vertices)
    vertices = vertices - center
    xySpan = vertices.ptp(0)
    vertices /= xySpan
    predictedPins = vertices[predictedPinIdxs]
    truePins = vertices[truePinIdxs]
    predStats = getTreeStats(vertices, predictedPinIdxs, truePinIdxs)
    trueStats, keptIdxs = getTreeStats(
        vertices, truePinIdxs, predictedPinIdxs, returnOriginalIndexers=True
    )

    # Find out how many predicted points were > 1 index away from truth, since
    # distance tolerance from other KD-Trees might be misleading for those cases
    # No need to check points with 0 error, since they are already known to align

    checkMask = trueStats > 0
    predPinsToCheck = predictedPins[keptIdxs][checkMask]
    truePinsToCheck = truePins[trueStats.index[checkMask]]
    assert len(predPinsToCheck) == len(
        truePinsToCheck
    ), f"{len(predPinsToCheck)} != {len(truePinsToCheck)}"
    verticesTree = KDTree(vertices)
    _, trueIndices = verticesTree.query(truePinsToCheck)
    _, predIndices = verticesTree.query(predPinsToCheck)

    return dict(
        error=np.sum(trueStats),
        extra=len(predictedPins) - len(predStats),
        missing=len(truePins) - len(trueStats),
        total=len(truePins),
    )


def getTreeStats(vertices, treePointIdxs, queryPointIdxs, returnOriginalIndexers=False):
    """
    Return total error distance and missing points from a KD-Tree query.

    If `returnOriginalIndexers` is specified, another array is returned which
    gives the indices of the returned samples relative to the initial query points
    """
    treePointIdxs = np.asarray(treePointIdxs)
    numVerts = len(vertices)

    def dist(kNum):
        treePts = treePointIdxs[indices[:, kNum]]
        queryPts = queryPointIdxs[mask]
        x1 = np.minimum(treePts, queryPts)
        x2 = np.maximum(treePts, queryPts)
        return np.minimum(x2 - x1, (x1 - x2) % numVerts)

    tree = KDTree(vertices[treePointIdxs])
    distances, indices = tree.query(vertices[queryPointIdxs], k=2)
    # Prioritize closer indices for each pair of matches
    mask = finiteMask = np.isfinite(distances).all(axis=1)
    indices = indices[mask]
    distances = distances[mask]
    queryPointIdxs = np.asarray(queryPointIdxs)
    idxDists = np.c_[dist(0), dist(1)]
    mask = np.argmin(idxDists, axis=1)
    distances = distances[np.arange(len(distances)), mask]
    indices = indices[np.arange(len(distances)), mask]
    uniques, counts = np.unique(indices, return_counts=True)
    keeps = np.ones(distances.size, bool)
    for idx in uniques[counts > 1]:
        checkLocs = indices == idx
        newValues = np.zeros(np.count_nonzero(checkLocs), bool)
        # Only the smallest distance match is kept
        newValues[np.argmin(distances[checkLocs])] = True
        keeps[checkLocs] = newValues
    # Now that antipins are handled, turn indices back into normal array rather than
    # interleaved
    # assert np.all((indices % 2) == 0), 'Odd index encountered'
    # indices //= 2
    ret = pd.Series(distances[keeps], indices[keeps])
    if returnOriginalIndexers:
        indexer = finiteMask
        indexer[finiteMask] = keeps
        return ret, np.flatnonzero(indexer)
    return ret


def errorForFile(groundTruthFile, predFile=None, outputFile=None):
    gtDf = GroundTruthMarker.readDataset(groundTruthFile)
    gtDf = gtDf[~gtDf["Flagged"].to_numpy(bool)]
    # gtDf = gtDf.iloc[[113]]
    if predFile is None:
        predPinIdxs = getDfPinPoints(gtDf)
    else:
        predPinIdxs = GroundTruthMarker.readDataset(predFile)[GroundTruthMarker.pinCol]
    assert len(predPinIdxs) == len(
        gtDf
    ), "Prediction and ground truth files must have the same number of rows"
    errs = []
    iterlist = zip(gtDf.to_dict("records"), predPinIdxs)
    for trueRow, predIdxs in iterlist:
        vertices = trueRow["Vertices"].stack()
        truePinIdxs = trueRow[GroundTruthMarker.pinCol]
        errStats = normedCoordError(vertices.view(np.ndarray), predIdxs, truePinIdxs)
        errs.append(errStats)
    errs = pd.DataFrame(errs)
    if outputFile:
        errs.to_csv(outputFile)
    return errs


def plotQueryPts(vertices, treePts, queryPts):
    pw = pg.PlotWidget()
    antiSpi = pg.ScatterPlotItem(
        *treePts[1::2].T, symbol="x", size=10, pen="r", brush="r"
    )
    pinSpi = pg.ScatterPlotItem(*treePts[::2].T, pen="r", brush="r", size=10)
    querySpi = pg.ScatterPlotItem(*queryPts.T, pen="k", brush="k", size=7)
    # Close the curve
    vertices = np.r_[vertices, vertices[[0]]]
    vertsCurve = pg.PlotCurveItem(*vertices.T, pen=pg.mkPen("b", width=2.5))
    for item in vertsCurve, antiSpi, pinSpi, querySpi:
        pw.addItem(item)
        if isinstance(item, pg.ScatterPlotItem):
            item.setOpacity(0.5)
    pw.setAspectLocked(True)
    pw.setBackground("w")
    pw.show()
    pg.exec()
    return pw


def plotDatasetStats(fileOrDf: str | Path | pd.DataFrame, show=True):
    if not isinstance(fileOrDf, pd.DataFrame):
        fileOrDf = GroundTruthMarker.readDataset(fileOrDf)
    # Rename just for clarity during reindexing
    df = fileOrDf[~(fileOrDf["Flagged"].to_numpy(bool))]
    # -----
    # Histogram of number of pins per sample
    # -----
    lens = df["Pins"].apply(len).value_counts()
    pinCountHistogram(lens)

    # representativeSampsFig(df)

    # -----
    # Number of sides with pins
    # -----
    aspectRatios, numPinSides = getPinSideStats(df, plot=False)

    # -----
    # Aspect ratio histogram
    # -----
    aspectRatiosFig(aspectRatios, df, numPinSides)

    if show:
        plt.show()


def pinCountHistogram(lens):
    plt.figure()
    trueTicks = np.array([2**n for n in range(1, 9)])
    binnedIdxs = np.digitize(lens.index, trueTicks)
    binnedIdxs = np.clip(binnedIdxs, 1, len(trueTicks)) - 1
    counts = np.zeros(trueTicks.size)
    for idx, value in zip(binnedIdxs, lens):
        counts[idx] += value
    logBins = np.log2(trueTicks)
    plt.bar(logBins, counts / counts.sum() * 100, width=0.8)
    plt.gca().set_xticks(logBins)
    plt.gca().set_xticklabels(trueTicks)
    plt.xlabel("# Pins")
    plt.ylabel("% Samples")


def aspectRatiosFig(aspectRatios, df, numPinSides):
    lens = df["Pins"].apply(len)
    plt.figure()
    shapes = "+ox^"
    for numsides in range(1, 5):
        mask = numPinSides == numsides
        plt.scatter(
            lens[mask],
            aspectRatios[mask],
            c=[numsides] * mask.sum(),
            vmin=1,
            vmax=4,
            marker=shapes[numsides - 1],
            label=f"{numsides} pinned sides",
            facecolors=None,
        )
    ax = plt.gca()
    ax.set_xscale("log")
    ax.set_yticks(range(1, 5))
    for func in ax.set_xticks, ax.set_xticklabels:
        func([4, 8, 16, 64, 128, 256])
    plt.xlabel("# Pins")
    plt.ylabel("Aspect Ratio")
    plt.legend()


def representativeSampsFig(df):
    plt.figure()
    numReps = 12
    rng = np.random.default_rng(seed=12)
    indices = rng.choice(df.index, numReps)
    fig, axs = plt.subplots(2, numReps // 2)
    axs = axs.flatten()
    for ii, row in enumerate(df.loc[indices].to_dict("records")):
        verts = row["Vertices"].stack().view(np.ndarray)
        span = verts.ptp(0)
        if span[0] < span[1]:
            verts = verts[:, ::-1]
        verts = np.r_[verts, verts[[0]]]
        axs[ii].plot(*verts.T, c="k")
        axs[ii].axis("equal")
        axs[ii].axis("off")
    fig.tight_layout()


def getPinSideStats(df, plot=False):
    pinSideStats = defaultdict(int)
    aspectRatios = np.empty(len(df))
    numPinSides = np.empty(len(aspectRatios), dtype=int)
    warnings.simplefilter("error", RuntimeWarning)
    for ii, row in enumerate(df.to_dict("records")):
        # Override default pins with file's pin locations
        verts = row["Vertices"].stack().view(np.ndarray)
        # Enclose the shape
        verts = np.r_[verts, verts[[0]]]
        _, angles = unrotate(verts, returnAngles=True, missingSideOk=True)
        # 90, 90, 90, 180, 180, 180 -> 2
        # since there are 2 unique pin angles
        pinAngles = angles[row["Pins"]]
        curNumPinSides = len(np.unique(pinAngles))
        pinSideStats[curNumPinSides] += 1

        xySpan = verts.ptp(0)
        # Vertical and horizontal components are equivalent
        # to the PinPoint algorithm, so ensure there isn't
        # spurrious variety in calculated ratios
        if xySpan[1] > xySpan[0]:
            xySpan = xySpan[::-1]
        aspectRatios[ii] = xySpan[0] / xySpan[1]
        numPinSides[ii] = curNumPinSides
    pinSideStats = pd.Series(pinSideStats)
    if plot:
        plt.figure()
        (pinSideStats / pinSideStats.sum() * 100).sort_index().plot.bar()
        plt.xlabel("# Sides")
        plt.ylabel("% Samples")
    return aspectRatios, numPinSides


def main_cli():
    import argparse

    parser = argparse.ArgumentParser(
        description="Calculate error metrics for a set of predictions"
    )
    parser.add_argument("groundTruth", help="Ground truth file")
    parser.add_argument(
        "--predictions", "-p", help="Predictions file", required=False, default=None
    )
    parser.add_argument(
        "--output", "-o", help="Output file", required=False, default=None
    )
    args = parser.parse_args()
    errorForFile(args.groundTruth, args.predictions, args.output)


if __name__ == "__main__":
    pg.mkQApp()
    base = Path(r"C:\Users\Nathan\Desktop\pinpoint-data\data")
    # base = Path(r'C:\Users\Nathan\Downloads')
    file = base / "ics_jacob.csv"
    plt.rcParams["pdf.fonttype"] = 42
    plt.rcParams["ps.fonttype"] = 42
    plt.rcParams["font.size"] = 16
    # plotDatasetStats(file)
    # return
    err_df = errorForFile(file, outputFile=file.parent / (file.stem + "_err.csv"))
    summary = err_df.sum(axis=0)
    summary["missing + extra"] = total_errs = summary["missing"] + summary["extra"]
    cols = list(summary.index)
    # Ensure "total" is last
    cols.remove("total")
    cols.append("total")
    summary = summary.loc[cols]
    summary["pct_accuracy"] = round(
        (summary["total"] - total_errs) / summary["total"] * 100, 1
    )
    summary = summary.rename({"total": "total # pins"})
    with pd.option_context("display.precision", 1):
        print(summary)
    # main_cli()
