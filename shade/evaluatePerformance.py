import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pyqtgraph as pg
from pandas import DataFrame as df
from skimage.measure import label, regionprops
from sklearn.metrics import confusion_matrix


def imshow_pg(img: np.ndarray):
    win = pg.PlotWindow()
    it = pg.ImageItem(img)
    win.addItem(it)


def getConfuseMat(truthLblImg: np.ndarray, genLblImg: np.ndarray):
    # Check both types of regions
    yTrue = []
    yPred = []
    for validLbl in [1, 2]:
        bwImg = truthLblImg == validLbl
        regions = regionprops(label(bwImg), genLblImg)
        for region in regions:
            genLbl = np.max(region.intensity_image)
            yTrue.append(validLbl > 1)
            yPred.append(genLbl)
    confuseMat = confusion_matrix(yTrue, yPred, labels=[0, 1])
    outDf = df(
        confuseMat,
        columns=["Pred. Invalid", "Pred. Valid"],
        index=["Truly Invalid", "Truly Valid"],
    )
    return outDf


def evalPerformance(truthLblImgName: str, generatedLblImgName: str):
    truthLbls = cv.imread(truthLblImgName, cv.IMREAD_GRAYSCALE)
    validatingShadowsImg = cv.imread(generatedLblImgName, cv.IMREAD_GRAYSCALE)
    validatingShadowsImg = cv.resize(
        validatingShadowsImg, truthLbls.shape[::-1], interpolation=cv.INTER_NEAREST
    )

    numShadsRange = np.arange(5)
    predInvalidPerReqdShadows = np.empty((numShadsRange.size, 2))

    for numReqdShads in numShadsRange:
        genLbls = (truthLbls > 0) & (validatingShadowsImg >= numReqdShads)

        cfsnDf = getConfuseMat(truthLbls, genLbls)
        cfsnArr = cfsnDf.to_numpy()
        predInvalidPerReqdShadows[numReqdShads, :] = cfsnArr[:, 0] / cfsnArr.sum(1)
        total = np.sum(cfsnArr)
        correct = np.sum(np.diag(cfsnArr))
        print(cfsnDf)
        print("------------")
        print(f"Accuracy: {correct}/{total} = {correct/total*100:0.2f}%")
    np.save("./cfsnPerNumValidatingShadows.npy", predInvalidPerReqdShadows)
    return predInvalidPerReqdShadows


if __name__ == "__main__":
    mainPath = "../ContourData/"
    plotData = evalPerformance(
        mainPath + "labeledMat_bwcomps_fromGroundTruth.png",
        mainPath + "labeledMat_validatingShadows.png",
    )
    plt.plot(plotData[:, 0] * 100, "g-", label="% FP Reduction")
    plt.plot(plotData[:, 1] * 100, "r--", label="% TP Reduction")
    plt.xlabel("Number of required shadows")
    plt.xticks(np.arange(0, 5, dtype=int))
    plt.ylim([0, 100])
    yRng = np.arange(0, 125, 25, dtype=int)
    plt.yticks(yRng, labels=[f"{ii}%" for ii in yRng])
    plt.legend()
    plt.savefig("./statsPerReqdShads.pdf", pad_inches=0, bbox_inches="tight")
    plt.show()
