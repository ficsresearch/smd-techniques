import os
from pathlib import Path
from typing import Union

import cv2 as cv
import matplotlib.pyplot as plt
from getshadows import ShadowStack


def printSingleImg(folder: Union[str, Path, os.PathLike]):
    folder = Path(folder)
    labels = ["Non-validated components", "Validated components"]

    highResStack = ShadowStack.fromFolder(folder)
    # with open(folder/'stuff', 'rb') as ifile:
    #   shads, refImg, comps = pkl.load(ifile)
    # thetas = np.deg2rad([0, 90, 180, 270])
    # highResStack = ShadowStack('test', comps, refImg, thetas,
    #                            np.dstack(shads))
    highResImg = highResStack.refImg
    fullResShape = highResStack.refImg.shape
    for resizeRatio in [
        0.5
    ]:  # tqdm([0.15, 0.25, 0.33, 0.4, 0.5, 0.75], 'Resized Image'):
        curShadowStack = highResStack.resizedStack(resizeRatio, keepShadows=True)

        labeledMat, labelcolors = curShadowStack.findComponents()

        # Resize labeled image to overlay at full resolution
        labeledMat = cv.resize(
            labeledMat, fullResShape[:2][::-1], interpolation=cv.INTER_NEAREST
        )
        fig, ax = plt.subplots()
        ax: plt.Axes
        ax.imshow(highResImg)
        # cmap = LinearSegmentedColormap('validation', np.vstack([[[0,0,0,0]], labelcolors]))
        im = ax.imshow(labeledMat / labeledMat.max(), cmap="viridis", alpha=0.75)
        fig.colorbar(im, orientation="horizontal")
        # for ii in range(2):
        #   plt.scatter([], [], c=labelcolors[[ii]], label=labels[ii])
        # ax.legend()
        ax.axis("off")
        outDir = Path("./Results")
        outDir.mkdir(exist_ok=True, parents=True)
        plt.savefig(
            outDir / f"{folder.name}_{resizeRatio*100}%.pdf",
            bbox_inches="tight",
            pad_inches=0,
            dpi=300,
        )


if __name__ == "__main__":
    curFolder = "C:/Users/njessurun/Pictures/shadow"
    # curFolder = '../data/'
    for folder in Path(curFolder).glob("F*/"):
        printSingleImg(folder)
