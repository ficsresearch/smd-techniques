from __future__ import annotations

from functools import lru_cache
from pathlib import Path
from typing import List

import cv2 as cv
import numpy as np
from pandas import DataFrame as df
from skimage import measure, morphology as morph
from skimage.exposure import equalize_hist, match_histograms
from tqdm import tqdm

tzip = lambda *args, desc=None, **kwargs: tqdm(
    zip(*args), desc=desc, total=len(args[0]), **kwargs
)

NChanImg = np.ndarray
BlackWhiteImg = np.ndarray
TwoDArray = np.ndarray


def _outerCallFunc(vdator: CompValidator, *args):
    return vdator(*args)


class ShadowStack:
    def __init__(
        self,
        name: str,
        refImg: NChanImg,
        illumImgStack: List[NChanImg] = None,
        illumAngles: np.ndarray = None,
        shadows: List[BlackWhiteImg] = None,
        validators=None,
    ):
        self.name = name
        self.refImg = refImg
        if validators is None:
            validators = []
            if shadows is None:
                shadows = [None] * len(illumAngles)
            for img, angle, shadow in zip(illumImgStack, illumAngles, shadows):
                validators.append(CompValidator(img, angle, shadow))
        self.validators = validators

    @classmethod
    def fromFolder(cls, folder: Path, newSzRatio=1.0, numOutputChannels=3):
        origName = str(next(folder.glob("orig*")))
        refImg = cv.imread(origName)
        refImg: np.ndarray = cv.cvtColor(refImg, cv.COLOR_BGR2RGB)

        if numOutputChannels == 1:
            clrArg = cv.COLOR_BGR2GRAY
        else:
            clrArg = cv.COLOR_BGR2RGB
        readFn = lambda imName: cv.cvtColor(cv.imread(str(imName)), clrArg)

        angleImNames = list(folder.glob("angle*"))
        angledIllumImgs = []

        outAngles = []
        for name in angleImNames:
            angledIllumImgs.append(readFn(name))
            # Every file in this array should be named angle___.ext
            # Grab the ___, which has the angle in degrees
            angle = float(name.stem.strip("angle"))
            outAngles.append(angle)
        newStack = cls(
            folder.stem, refImg, angledIllumImgs, np.deg2rad(outAngles), shadows=None
        )
        if newSzRatio == 1:
            return newStack
        else:
            return newStack.resizedStack(newSzRatio)

    def resizedStack(self, resizeRatio: float, keepShadows=False):
        if resizeRatio == 1:
            listCpy = lambda lst: [el.copy() for el in lst]
            return ShadowStack(
                self.name, self.refImg.copy(), validators=listCpy(self.validators)
            )
        fullResShape = self.refImg.shape[:2]
        newSz = tuple((np.array(fullResShape) * resizeRatio).round().astype(int))

        if self.validators is not None:
            newValidators = [
                vdator.resize(newSz[::-1], keepShadow=keepShadows)
                for vdator in self.validators
            ]
        else:
            newValidators = None
        newRefIm = cv.resize(self.refImg, newSz[::-1], cv.INTER_AREA)
        return ShadowStack(self.name, newRefIm, validators=newValidators)

    @property
    @lru_cache(maxsize=None)
    def minElSz(self):
        return max(
            1, int(np.round(10 / (1920 * 1080) * np.prod(self.refImg.shape[:2])))
        )

    def findComponents(self):
        radius = int(np.sqrt(self.minElSz))
        bwcomps, bgcolor = bwBgMask(cv.GaussianBlur(self.refImg, None, 1), radius)

        labelcolors = (
            np.array([[255, 0, 0, 255], [100, 255, 0, 255], [0, 0, 255, 255]]) / 255
        )
        if bgcolor == "black":
            bgcolor = "green"
        xpondingClrs = ["red", "green", "blue"]
        labelcolors = labelcolors[~np.isin(xpondingClrs, bgcolor)]

        # Do vdator processing
        # Number of vdator associations required to validate a component
        numReqdShadows = len(self.validators) // 2
        # Use distance transform as a metric to attach each vdator to its closest
        # component
        # Bwcomps represents foregrounds, distance transform finds distance to closest
        # foreground component. For the purposes of SHADE I want this distance to be
        # nonzero for each non-foregroudn area, so invert before performing distance transform
        dists, distIdxs = cv.distanceTransformWithLabels(
            (~bwcomps).astype("uint8"),
            cv.DIST_L1,
            self.minElSz,
            labelType=cv.DIST_LABEL_CCOMP,
        )
        bwCloseDists = (dists > 0) & (dists <= self.minElSz)

        # validComps = np.zeros_like(bwcomps, dtype=int)
        coordList = measure.regionprops_table(
            measure.label(bwcomps), properties=("coords",)
        )["coords"]

        # Workhorse of the function -- code adapted from https://stackoverflow.com/a/37060925/9463643
        # normedRefImg = intensityNorm(self.refImg)
        procArgs = (distIdxs, bwCloseDists, coordList, self.refImg)
        # validComps = mproc_apply(_outerCallFunc, self.validators, 'Validating Shadows', extraArgs=procArgs)
        validComps = []
        for vdator in tqdm(
            self.validators, desc="Validating shadows"
        ):  # type: CompValidator
            validComps.append(vdator(*procArgs))

        validComps = sum(validComps)
        labelMat = bwcomps.astype(int)
        labelMat += validComps  # >= numReqdShadows
        return labelMat, labelcolors


class CompValidator:
    def __init__(
        self, illumImg: NChanImg, theta_rad: np.float, shadow: BlackWhiteImg = None
    ):
        self.illumImg = illumImg
        self.theta_rad = theta_rad
        self.shadow = shadow

    def copy(self):
        shadowCpy = None if self.shadow is None else self.shadow.copy()
        return CompValidator(self.illumImg.copy(), self.theta_rad, shadowCpy)

    def resize(self, newXYSz=(0, 0), newSzRatio=None, keepShadow=True):
        if newXYSz == (0, 0) and newSzRatio is None:
            return self.copy()
        cvResizeArgs = (newXYSz,)
        if newSzRatio is not None:
            cvResizeArgs += (None, newSzRatio, newSzRatio)  # dst=None
        if self.shadow is not None and keepShadow:
            resizedShad = cv.resize(
                self.shadow.astype("uint8"), *cvResizeArgs, interpolation=cv.INTER_AREA
            ).astype(bool)
        else:
            resizedShad = None
        resizedIllumImg = cv.resize(
            self.illumImg, *cvResizeArgs, interpolation=cv.INTER_AREA
        )
        return CompValidator(resizedIllumImg, self.theta_rad, resizedShad)

    def findShadows(self, refImg: NChanImg):
        illumImg_norm = equalize_hist(self.illumImg) * 255
        illumImg_matched = match_histograms(illumImg_norm, refImg)
        diffCmp = (illumImg_matched.astype(float) - illumImg_norm.astype(float)).mean(
            2
        ) > 0
        # In the diff img, shadows should be areas of low variance and high brightness
        # AND low brightness in illuminated image compared to everything else
        # valueChans = []
        # for img in illumImg_norm, illumImg_matched, refImg:
        #   valueChans.append(cv.cvtColor(img.astype('uint8'), cv.COLOR_RGB2HSV)[...,2].astype(float))

        # Diff between matched and normed give shadows
        # diff between matched/normed and ref give valid region (patchy when far from light)

        vThresh = cv.GaussianBlur(refImg, (0, 0), 6)

        validPixs = diffCmp & ((illumImg_norm - vThresh).mean(2) <= 0)
        return validPixs

    def findShadows_old(self, normedRefImg: NChanImg):
        # Light source is uneven across the image, so divide by intensity
        # estimate to normalize against this
        illumImg = intensityNorm(self.illumImg)

        # Shadows will always be darker than original image AND darker than a
        # given threshold. Obtain this threshold by determining how dark on average a pixel
        # is compared to its neighbors by strong gaussian filtering
        rgbThresh = cv.GaussianBlur(illumImg, (0, 0), 6)
        validPixs = (np.sum(illumImg < normedRefImg, 2) >= 2) & (
            np.sum(illumImg < rgbThresh, 2) >= 2
        )

        self.shadow = validPixs
        return validPixs

    def rmSmallShadows(self, minShadSz):
        validPixs = self.shadow
        if minShadSz > 0:
            props = df(
                measure.regionprops_table(
                    measure.label(validPixs), properties=("area", "coords")
                )
            )
            invalidCoords = props[props.area < minShadSz].coords
            if len(invalidCoords) > 0:
                invalidPixs = np.vstack(invalidCoords)
                validPixs[invalidPixs[:, 0], invalidPixs[:, 1]] = False
        return validPixs

    def __call__(
        self,
        distIdxs,
        bwCloseDists: BlackWhiteImg,
        compCoordList: np.ndarray,
        normedRefImg: NChanImg,
    ):
        if self.shadow is None:
            self.shadow = self.findShadows(normedRefImg)
        bwValid = np.zeros_like(self.shadow)
        theta = self.theta_rad
        closeRows, closeCols = np.nonzero(self.shadow & bwCloseDists)
        angles = np.sin(theta) * closeRows + np.cos(theta) * closeCols
        for coords in compCoordList:
            # Since CCOMP was specified, all distance idxs leading to this comp will
            # be the same. So, any arb. coord will give the right comp id
            rows, cols = coords.T
            regionId = distIdxs[rows[0], cols[0]]
            # Get the max angle value of the pixels of the current component to find
            # which shadow pixels to exclude
            angleOriginOffset = (np.sin(theta) * rows + np.cos(theta) * cols).max()
            # Also only consider close shadows that came from this component
            closeShadowIdxs = distIdxs[closeRows, closeCols] == regionId
            curAngles = angles[closeShadowIdxs] - angleOriginOffset
            considerValid = np.any(curAngles > 0)
            # If any *close* *shadows* are in the *right direction*, it's a valid component
            if considerValid:
                bwValid[rows, cols] = True
        return bwValid


def bwBgMask(img: np.ndarray, minShadSz):
    boardColors = ["red", "green", "blue"]
    [rr, gg, bb] = cv.split(img)
    primaryClrMult = 1.25

    # Guess background color from mean intensities per channel
    chans = img.reshape(-1, 3)
    chanMeans = np.mean(chans, 0)
    bgChan = np.argmax(chanMeans)
    val = chanMeans[bgChan]
    # Black background if low intensity and similar to other intensities
    if np.all(np.abs(val) - chanMeans < 10) & (val < 50):
        bgcolor = "black"
    else:
        bgcolor = boardColors[int(bgChan)]

    cmp = bgcolor.lower()
    if cmp == "black":
        # For black board
        bwBackground = img.mean(2) > 100
    elif cmp == "blue":
        bwBackground = (bb > primaryClrMult * gg) & (bb > primaryClrMult * rr)
    elif cmp == "red":
        bwBackground = (rr > primaryClrMult * gg) & (rr > primaryClrMult * bb)
    else:
        # Default to 'green'
        #     warning('Unexpected board color entered. Defaulting to green background assumption.')
        # Assume shadow will only be on circuit board pixels
        # Relationships determined by experimentation
        bwBackground = (gg > primaryClrMult * rr) & (gg > primaryClrMult * bb)
    bwBackground = morph.closing(bwBackground, np.ones((minShadSz,) * 2))
    return ~bwBackground, bgcolor


def intensityNorm(img: NChanImg):
    lightEstimate = cv.GaussianBlur(img, (0, 0), 6)
    if lightEstimate.ndim > 2:
        lightEstimate = np.mean(lightEstimate, 2, keepdims=True)

    img = img / lightEstimate
    img = img / img.max()
    # Experimentation showed blurring first yields a better shadow detection
    img = cv.blur(img, (5, 5))
    return img
