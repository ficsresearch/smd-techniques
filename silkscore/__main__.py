from qtextras import fns
from s3awfs import MainWorkflow

from . import (
    AnnotationCorrelatorWorkflow,
    AnnotationLinkerWorkflow,
    LocatorFeaturesWorkflow,
)


def makeMwf(output_dir):
    mwf = MainWorkflow(
        output_dir,
        [
            AnnotationLinkerWorkflow,
            LocatorFeaturesWorkflow,
            AnnotationCorrelatorWorkflow,
        ],
    )
    return mwf


def main(data_dir, output_dir):
    mwf = makeMwf(output_dir)
    mwf.activate(data_dir=data_dir)


if __name__ == "__main__":
    cli = fns.makeCli(main)
    # mwf.get(LocatorFeaturesWorkflow).plot_desig_palette()
    main(**vars(cli.parse_args()))
