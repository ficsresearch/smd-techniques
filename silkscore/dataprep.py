from __future__ import annotations

import shutil
from pathlib import Path
import typing as t

import cv2 as cv
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from qtextras import fns
from s3a import PRJ_ENUMS, ComponentIO, TableData, defaultIo
from s3awfs import WorkflowDirectory, constants as s3awfs_cnst
from s3awfs.utils import RegisteredPath
from tqdm import tqdm

TEMPLATES_DIR = Path(__file__).resolve().parent / "tblcfg"


class AnnotationLinkerWorkflow(WorkflowDirectory):
    all_ocrs_file = RegisteredPath(".csv")
    all_smds_file = RegisteredPath(".csv")
    ocr_dir = RegisteredPath()
    smd_dir = RegisteredPath()
    link_dir = RegisteredPath()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ocr_io = ComponentIO(TableData(TEMPLATES_DIR / "proj_ocr.s3aprj"))

    def runWorkflow(self, data_dir):
        data_dir = Path(data_dir)
        smd_dir = data_dir / "smd_annotation"
        ocr_dir = data_dir / "ocr_annotation"

        # Naming scheme is different between inputs and outputs
        ocr_files = fns.naturalSorted(
            f
            for f in ocr_dir.glob("*.csv")
            if not (self.ocr_dir / self._strip_ann_type(f.name)).exists()
        )
        for ocr_file in ocr_files:
            smd_file = smd_dir.joinpath(ocr_file.name.replace("ocr", "smd"))
            if not smd_file.exists():
                continue
            self.export_single_file_locators(ocr_file, smd_file)

    def export_single_file_locators(self, ocr_file, smd_file):
        # Only care about text with a single component locator on non-device classes
        # Blank component locators have the string "[]" so look for anything longer
        # than this
        initial_ocr_df = pd.read_csv(ocr_file, dtype=str, na_filter=False)
        ocr_df = initial_ocr_df.query(
            '(Class != "Device") & `Component Locator`.str.len() > 2'
        )
        # So far just a string dataframe, now need to import it to parse coordinates
        ocr_df = self.ocr_io.importCsv(ocr_df)
        # Filter out ambiguous cases (i.e. >=2 component locators)
        ocr_df = ocr_df[ocr_df["Component Locator"].apply(len) == 1].copy()

        # 'pcb_5f_cc_11_ocr.csv' -> 'pcb_5f_cc_11.csv', works for ocr and smd files alike
        save_name = self._strip_ann_type(ocr_file.name)
        if len(ocr_df):
            # There are some points to associate with smd
            smd_df = defaultIo.importCsv(smd_file, keepExtraFields=True)
            links = self.match_locator_to_smd(ocr_df, smd_df, keep_first=True)
            if len(links):
                initial_ocr_df["smd_id"] = pd.Series(
                    [str(l) if l else "" for l in links], index=links.index
                )
                initial_ocr_df.to_csv(self.ocr_dir / save_name, index=False)
                shutil.copy(smd_file, self.smd_dir / save_name)
                links_df = pd.DataFrame(
                    index=links.index, data=links, columns=["smd_id"]
                )
                links_df.to_csv(self.link_dir / save_name)  # index=True
                return links_df
        # Epxlicit return to indicate intentional return value
        return None

    @classmethod
    def _strip_ann_type(cls, ann_name: str | Path):
        if isinstance(ann_name, str):
            return ann_name.replace("_ocr", "").replace("_smd", "")
        return ann_name.with_name(cls._strip_ann_type(ann_name.name))

    @classmethod
    def match_locator_to_smd(cls, ocr_df, smd_df, match_margin=3, keep_first=False):
        smd_verts = smd_df["Vertices"].apply(lambda el: el.stack().astype("float32"))
        pt_matches = {}
        dtype = int if keep_first else object
        for idx, row in ocr_df[ocr_df["Component Locator"].apply(len) > 0].iterrows():
            smd_ids = cls.match_pts_to_smds(
                row["Component Locator"], smd_verts, match_margin
            )
            smd_ids = [id_ for id_ in smd_ids if id_ is not None]
            if not len(smd_ids):
                continue
            if keep_first:
                smd_ids = smd_ids[0]
            pt_matches[idx] = smd_ids
        ret = pd.Series(pt_matches, dtype=dtype)
        ret.index.name = "ocr_id"
        return ret

    @staticmethod
    def match_pts_to_smds(locator_pts, smd_verts, match_margin):
        smd_ids = []
        for check_pt in locator_pts.astype("float32"):
            best_dist = -np.inf
            best_smd_id = 0
            for smd_id, poly in smd_verts.items():
                dist = cv.pointPolygonTest(poly, check_pt, True)
                if dist > best_dist:
                    best_dist = dist
                    best_smd_id = smd_id
                    if dist > 0:
                        break
            if best_dist >= -match_margin:
                smd_ids.append(best_smd_id)
            else:
                smd_ids.append(None)
        return smd_ids

    def file_list(self, exclude_dir=None, prefix=None):
        files = fns.naturalSorted(f.name for f in self.ocr_dir.glob("*.csv"))
        if exclude_dir is not None:
            existing = {f.name for f in Path(exclude_dir).glob("*.csv")}
            files = [f for f in files if f not in existing]
        if prefix is not None:
            prefix = Path(prefix)
            files = [prefix / f for f in files]
        return files


class LocatorFeaturesWorkflow(WorkflowDirectory):
    num_means = 3

    features_dir = RegisteredPath()
    designator_colors_file = RegisteredPath(".csv")
    designator_palette_file = RegisteredPath(".svg")
    linker: AnnotationLinkerWorkflow

    def runWorkflow(self, data_dir, plot_palettes=False, num_means=None):
        self.linker = self.parent().get(AnnotationLinkerWorkflow)
        im_dir = Path(data_dir) / "pcb_image"
        if num_means is not None:
            type(self).num_means = num_means
        fns.multiprocessApply(
            self.export_single_feature_df,
            self.linker.file_list(exclude_dir=self.features_dir),
            "Creating feature files",
            im_dir=im_dir,
            debug=s3awfs_cnst.DEBUG,
        )
        self.create_desig_color_feats(im_dir)
        if plot_palettes:
            self.plot_desig_palette()

    def export_single_feature_df(self, file, im_dir=None):
        im_dir = Path(im_dir or "")
        smd_df = defaultIo.importCsv(self.linker.smd_dir / file)
        ocr_df = self.linker.ocr_io.importCsv(
            self.linker.ocr_dir / file, keepExtraFields=True
        )
        ocr_df["smd_id"] = [int(id_) if id_ else np.nan for id_ in ocr_df["smd_id"]]

        keep_ids = ocr_df["smd_id"].dropna().unique().astype(int)
        smd_image_df = defaultIo.exportCompImgsDf(
            smd_df.loc[keep_ids],
            source=im_dir,
            resizeOptions=dict(rotationDegrees=PRJ_ENUMS.ROTATION_OPTIMAL),
        )
        feature_df = self.extract_link_features(ocr_df, smd_image_df, im_dir)
        feature_df.to_csv(self.features_dir / file, float_format="%.2f")

    @classmethod
    def extract_link_features(
        cls,
        ann_df: pd.DataFrame,
        smd_image_df: pd.DataFrame = None,
        im_dir=None,
        compute_image_df=True,
    ):
        feature_cols = ["orientation", "centroid_x", "centroid_y"]
        if smd_image_df is None and compute_image_df:
            smd_image_df = defaultIo.exportCompImgsDf(
                ann_df,
                source=im_dir,
                resizeOptions=dict(rotationDegrees=PRJ_ENUMS.ROTATION_OPTIMAL),
            )
        if compute_image_df:
            # r_0, g_0, b_0, r_1, g_1, b_1, pct_0, pct_1
            # 'pct' is the percentage of pixels which have that mean
            feature_cols += cls.color_feature_names()
        prealloc_data = np.full((len(ann_df), len(feature_cols)), -1).astype(float)
        feature_df = pd.DataFrame(
            prealloc_data, columns=feature_cols, index=ann_df.index
        )

        for idx, row in ann_df.iterrows():
            row = t.cast(dict, row)
            smd_id = t.cast(int, row.get("smd_id", idx))
            if not np.isfinite(smd_id):
                continue
            smd_id = int(smd_id)
            if smd_image_df is not None:
                smd_row = smd_image_df.loc[smd_id]
                smd_image = smd_row["image"]
                smd_mask = smd_row["labelMask"]
                means_data = cls.get_color_features(smd_image[smd_mask > 0, :])
            else:
                means_data = []
            center, w_h, angle = cv.minAreaRect(row["Vertices"][0].astype("float32"))
            # See column names for each feature description
            if "Orientation" in row:
                orient = row["Orientation"]
            else:
                orient = angle
            while orient < 0:
                # Orientations are symmetric when flipped 180 degrees
                orient += 180
            feature_df.loc[idx] = [orient % 90, *center, *means_data]
        return feature_df

    @classmethod
    def color_feature_names(cls):
        means_cols = []
        for lbl in range(cls.num_means):
            # Assume rgb images
            means_cols.extend(f"r_{lbl} g_{lbl} b_{lbl}".split())
        for lbl in range(cls.num_means):
            means_cols.append(f"pct_{lbl}")
        return means_cols

    @classmethod
    def get_color_features(cls, smd_pixels, as_dict=False):
        out = cls._kmeans(smd_pixels, k=cls.num_means)
        uniques, counts = np.unique(out["labels"], return_counts=True)
        order = np.argsort(counts)[::-1]
        uniques = uniques[order]
        counts = counts[order]
        out_size = out["labels"].size
        membership_pct = [count / out_size for count in counts]
        feats = out["means"][uniques].ravel().astype(int).tolist() + membership_pct
        if not as_dict:
            return feats
        return {col: f for col, f in zip(cls.color_feature_names(), feats)}

    @staticmethod
    def _kmeans(reshaped_pixels, k=3):
        clrs = reshaped_pixels.astype("float32")
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        # Name the variable for clarity
        attempts = 20
        ret, lbls, means = cv.kmeans(
            clrs, k, None, criteria, attempts, cv.KMEANS_RANDOM_CENTERS
        )
        return dict(labels=lbls, means=means)

    def create_desig_color_feats(self, im_dir=None, skip_existing=True):
        if skip_existing and self.designator_colors_file.exists():
            # print("Aggregate designator colors already exist; skipping generation")
            return
        all_smds_df = pd.concat(
            [
                pd.read_csv(self.linker.smd_dir / f, dtype=str, na_filter=False)
                for f in self.linker.file_list()
            ]
        )
        # Limit to X max per designator type since this is just used to grab a few
        # primary colors
        # 1000s of samples are not needed for this
        max_samps = 150
        desig_colors_df = pd.DataFrame(columns=self.color_feature_names())
        for desig, subdf in tqdm(
            all_smds_df.groupby("Designator"), "Creating aggregate designator colors"
        ):
            if len(subdf) > max_samps:
                subdf = subdf.sample(n=max_samps)
            # Ensure ids are unique for image creation in feature workflow
            subdf.index = subdf["Instance ID"] = np.arange(len(subdf))
            subdf = defaultIo.importCsv(subdf, keepExtraFields=True)
            feats = self.extract_link_features(subdf, im_dir=im_dir)
            desig_colors_df.loc[desig] = np.median(
                feats[desig_colors_df.columns], axis=0
            )
            # Save every iteration to get faster updates
            desig_colors_df.to_csv(self.designator_colors_file, float_format="%.2f")

    def plot_desig_palette(self):
        df = pd.read_csv(self.designator_colors_file, index_col=0, na_filter=False)
        colors = df.filter(regex=r"^\w_\d+$").to_numpy(int)
        out_width = int(len(colors) / 3)
        pcts = (df.loc[:, "pct_0":] * out_width).to_numpy(int)
        out_colors = np.empty((len(colors), out_width, 3), dtype=int)
        for ii, (row_clr, c_pct) in enumerate(zip(colors, pcts)):
            start = 0
            clr = 0
            for clr, pct in zip(row_clr.reshape(-1, 3), c_pct):
                out_colors[ii, start : start + pct, :] = clr
                start += pct
            # Happens when percents don't round nicely. Just give remaining membership
            # to last percentage
            if start < out_width:
                out_colors[ii, start:, :] = clr
        max_lbl_len = max(3, df.index.str.len().max())
        labels = df.index.to_numpy(f"<U{max_lbl_len}")
        labels[labels == ""] = "</>"
        self._canvas_from_palette(out_colors, labels, self.designator_palette_file)

    @staticmethod
    def _canvas_from_palette(img, palette_names, save_name=None):
        # Credit: https://stackoverflow.com/a/16061699/9463643
        # get the dimensions
        ypixels, xpixels, bands = img.shape

        # get the size in inches
        dpi = 5.0
        xinch = xpixels / dpi
        yinch = ypixels / dpi

        # plot and save in the same size as the original
        fig: plt.Figure = plt.figure(figsize=(xinch, yinch))
        fig.subplots_adjust(0, 0, 1, 1)

        fig.gca().imshow(img, interpolation="none")
        plt.yticks(np.arange(len(img), dtype=float), palette_names)
        plt.xticks([])
        plt.tight_layout()
        if save_name:
            plt.savefig(save_name, bbox_inches="tight")
        return fig
