from __future__ import annotations

import re
from pathlib import Path

import numpy as np
import pandas as pd
import scipy.spatial
from qtextras import fns
from s3a import XYVertices, defaultIo
from s3awfs import WorkflowDirectory, constants as s3awfs_cnst
from s3awfs.utils import RegisteredPath
from skimage import feature

from silkscore.dataprep import AnnotationLinkerWorkflow, LocatorFeaturesWorkflow


class AnnotationCorrelatorWorkflow(WorkflowDirectory):
    ocr_dir = RegisteredPath()
    link_dir = RegisteredPath()

    feature_wf: LocatorFeaturesWorkflow
    linker: AnnotationLinkerWorkflow

    desig_regex = re.compile(r"^([A-Z])\d")

    def runWorkflow(
        self,
        data_dir,
        use_color_features=False,
        use_orientation_feature=False,
    ):
        self.linker = self.parent().get(AnnotationLinkerWorkflow)
        self.feature_wf = self.parent().get(LocatorFeaturesWorkflow)

        files = self.linker.file_list(exclude_dir=self.ocr_dir)
        fns.multiprocessApply(
            self._run_wf_single,
            files,
            im_dir=Path(data_dir) / "pcb_image",
            debug=s3awfs_cnst.DEBUG,
            use_color_features=use_color_features,
            use_orientation_feature=use_color_features,
        )

    def _run_wf_single(
        self, file, im_dir, use_color_features=True, use_orientation_feature=True
    ):
        self.correlate_ocr_smd(
            self.linker.ocr_dir / file,
            self.linker.smd_dir / file,
            im_dir,
            use_color_features,
            use_orientation_feature,
        )

    def correlate_ocr_smd(
        self,
        ocr_file,
        smd_file,
        im_dir,
        use_color_features=True,
        use_orientation_feature=True,
    ):
        ocr_df = self.linker.ocr_io.importCsv(ocr_file)
        if "smd_id" in ocr_df:
            ocr_df["smd_id"] = [int(id_) if id_ else np.nan for id_ in ocr_df["smd_id"]]

        ocr_df["Component Locator"] = [XYVertices() for _ in range(len(ocr_df))]
        desig_color_feats = pd.read_csv(
            self.feature_wf.designator_colors_file, index_col=0, na_filter=False
        )
        smd_df = defaultIo.importCsv(smd_file)

        smd_feats_df = self.feature_wf.extract_link_features(
            smd_df, im_dir=im_dir, compute_image_df=use_color_features
        )
        ocr_df_with_device = ocr_df
        ocr_df = ocr_df.query('Class != "Device"')
        desigs = ocr_df["Text"].apply(self.text_to_designator)
        ocr_feats_df = self.feature_wf.extract_link_features(
            ocr_df, compute_image_df=False, im_dir=im_dir
        )
        # smd_feats_df = smd_feats_df.filter(like="centroid")
        # ocr_feats_df = ocr_feats_df.filter(like="centroid")
        # "to_numpy" avoids keeping designator indices
        if use_color_features:
            floatVals = desig_color_feats.reindex(desigs).to_numpy(float)
            floatVals[np.isnan(floatVals)] = -1
            ocr_feats_df[desig_color_feats.columns] = floatVals

        assert list(ocr_feats_df) == list(smd_feats_df), "len SMD != OCR feats"

        if not use_orientation_feature:
            ocr_feats_df = self.remove_columns_from_df(ocr_feats_df, "orientation")
            smd_feats_df = self.remove_columns_from_df(smd_feats_df, "orientation")
        matches_ser = self.match_descriptors(ocr_feats_df, smd_feats_df)

        # Recover initial dataframe with all values
        ocr_df = ocr_df_with_device
        ocr_df["smd_id"] = ""
        ocr_df.loc[matches_ser.index, "smd_id"] = matches_ser.to_numpy(str)
        ocr_df.loc[matches_ser.index, "Component Locator"] = [
            XYVertices([row])
            for row in smd_feats_df.loc[
                matches_ser.to_numpy(int), ["centroid_x", "centroid_y"]
            ].values
        ]
        self.linker.ocr_io.exportCsv(ocr_df, self.ocr_dir / Path(ocr_file).name)

        links_df = pd.DataFrame(matches_ser)
        links_df.to_csv(self.link_dir / Path(ocr_file).name)

    @staticmethod
    def remove_columns_from_df(dataframe: pd.DataFrame, columns: str | list[str]):
        if isinstance(columns, str):
            columns = [columns]
        keep = [col for col in dataframe if col not in columns]
        return dataframe[keep]

    @classmethod
    def _match_kdtree(cls, ocr_feats, smd_feats):
        tree = scipy.spatial.KDTree(ocr_feats)
        dists, matches = tree.query(smd_feats)
        keeps = np.isfinite(dists)
        ocr_smd_map = np.c_[matches, np.arange(len(matches))]
        ocr_smd_map = ocr_smd_map[keeps]
        # Reindex to match full dataframes
        return cls._series_from_ocr_smd_ids(ocr_smd_map, smd_feats, ocr_feats)

    @staticmethod
    def _series_from_ocr_smd_ids(ocr_smd_map, ocr_feats, smd_feats):
        ret = pd.Series(
            data=smd_feats.index[ocr_smd_map[:, 1]],
            index=ocr_feats.index[ocr_smd_map[:, 0]],
        )
        ret.index.name = "ocr_id"
        ret.name = "smd_id"
        return ret

    @classmethod
    def match_descriptors(cls, ocr_feats, smd_feats):
        desc_mapping = feature.match_descriptors(ocr_feats.values, smd_feats.values)
        return cls._series_from_ocr_smd_ids(desc_mapping, ocr_feats, smd_feats)

    def text_to_designator(self, text):
        match = re.search(self.desig_regex, text)
        if match:
            return match.group(1)
        # No group = ""
        return ""
