## Dropbox link to `FPIC`
https://www.dropbox.com/sh/8xxncmghvb1a4sr/AACO7DJIL-gNHNpWR1eis2P8a?dl=0

## Silkscreen Correlation (SilkScore) Code
https://gitlab.com/ficsresearch/smd-techniques/-/tree/master/silkscore

## Setup SilkScore
See installation instructions from top-level readme.

## Running SilkScore
- See [`__main__.py`](https://gitlab.com/ficsresearch/smd-techniques/-/blob/master/silkscore/__main__.py) for details.
  - Provide a `--data_dir` argument which points to the location of FPIC. SilkScore
    will search this directory for `smd_annotation`, `ocr_annotation`, and `pcb_image`
    subdirectories.
  - Provide a `--output_dir` argument which points to the location where you want
    to save the output. SilkScore will create several folders with various outputs in
    this location.

- With these arguments in mind, you can run `silkscore` as a module:
```bash
python -m silkscore --data_dir <path/to/fpic> --output_dir <path/to/output>
```
  - <span style="color:red">**Be warned: Generating features can take some time
    (~30 minutes on my machine)**.</span>

- The final outputs are under the `Annotation Correlator` folder of your specified
  output location.
  - `silkscore` doesn't re-calculate any files that were already processed; simply
    delete them from their respective output directories to force a recalculation.