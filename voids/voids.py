import typing as t
from pathlib import Path

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyqtgraph as pg
from pyefd import elliptic_fourier_descriptors
from s3a import REQD_TBL_FIELDS as RTF, ComplexXYVertices, ProjectData
from s3a.views.regions import MultiRegionPlot
from s3awfs import MainWorkflow, WorkflowDirectory
from s3awfs.utils import RegisteredPath
from scipy import ndimage as ndi
from skimage import exposure, io, util
from skimage.measure import label, regionprops, regionprops_table
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

here = Path(__file__).resolve().parent


class AnnotationReaderWorkflow(WorkflowDirectory):
    def runWorkflow(
        self, project_path=here / "voids.s3aprj", n_largest_components=60, plot=False
    ):
        n_largest_components = n_largest_components

        prj = ProjectData(project_path)

        text_anns = pd.concat(
            [
                pd.read_csv(f, dtype=str, na_filter=False)
                for f in prj.imageAnnotationMap.values()
            ]
        )

        imported_anns = prj.componentIo.importCsv(text_anns, reindex=True)
        sort_order = (
            imported_anns["Vertices"]
            .apply(lambda el: cv.contourArea(el.stack()))
            .sort_values(ascending=False)
        )
        use_indices = sort_order.index[:n_largest_components]

        to_convert = imported_anns.loc[use_indices]
        image_df = prj.componentIo.exportCompImgsDf(to_convert)

        if not plot:
            return dict(image_df=image_df, annotations_df=imported_anns)
        self.plot_largest_samples_and_cutoff(image_df, n_largest_components, sort_order)
        self.show_annotations_on_image(prj, use_indices)

        return dict(image_df=image_df, annotations_df=imported_anns)

    @staticmethod
    def show_annotations_on_image(prj, use_indices):
        pg.mkQApp()
        pw = pg.PlotWidget()
        pw.setAspectLocked(True)
        image, ann_file = next(iter(prj.imageAnnotationMap.items()))
        pw.addItem(pg.ImageItem(io.imread(image)))
        mrp = MultiRegionPlot()
        mrp.resetRegionList(prj.componentIo.importCsv(ann_file))
        mrp.selectById(use_indices)
        pw.addItem(mrp)
        pw.show()
        pw.raise_()

    @staticmethod
    def plot_largest_samples_and_cutoff(image_df, n_largest_components, sort_order):
        plt.figure()
        plt.plot(sort_order.values)
        plt.scatter(n_largest_components, sort_order.iloc[n_largest_components])
        plt.title("Contour area & cutoff point")
        fig, ax = plt.subplots(1, figsize=(10, 5), dpi=100)
        plt.title("Representative samples")
        sample_images = image_df["image"].apply(lambda el: cv.resize(el, (256, 256)))
        all_stacked = np.stack(sample_images)
        ax.imshow(util.montage(all_stacked))
        return fig


class ThresholdWorkflow(WorkflowDirectory):
    component_images_dir = RegisteredPath()
    solder_pad_masks_dir = RegisteredPath()
    solder_void_masks_dir = RegisteredPath()

    summary_file = RegisteredPath(".html")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def name_folder_map(self):
        return dict(
            image=self.component_images_dir,
            labelMask=self.solder_pad_masks_dir,
            thresholded=self.solder_void_masks_dir,
        )

    @staticmethod
    def default_threshold(image, mask, size_threshold=8):
        image = ndi.grey_opening(image, size=(3, 3))
        bool_mask = ndi.binary_erosion(mask > 0)
        img_lt_med = image < np.percentile(image, 25)
        thresholded = bool_mask & img_lt_med
        regions = regionprops(label(thresholded))
        for region in regions:
            if region.area <= size_threshold:
                thresholded[region.coords[:, 0], region.coords[:,1]] = False
        return thresholded

    def runWorkflow(
        self,
        image_df: pd.DataFrame,
        threshold_function: t.Callable[[np.ndarray, np.ndarray], np.ndarray] = None,
        save_to_disk=False,
    ):
        if threshold_function is None:
            threshold_function = self.default_threshold
        image_df = image_df.copy()
        image_df["thresholded"] = None

        for ii, row in image_df.iterrows():
            image_name = f"{ii}.png"
            image_df.at[ii, "thresholded"] = row["thresholded"] = threshold_function(
                row["image"], row["labelMask"]
            )
            if not save_to_disk:
                continue
            for src_col, dest_dir in self.name_folder_map.items():
                dest_file = dest_dir / image_name
                assert row[src_col] is not None, str(ii)
                out_image = exposure.rescale_intensity(row[src_col], out_range="uint8")
                if out_image.ptp() == 0:
                    out_image[:] = np.iinfo(out_image.dtype).max
                io.imsave(dest_file, out_image, check_contrast=False)

        if save_to_disk:
            html_df = self.make_summary(image_df)
            self.summary_file.write_text(html_df)

        return dict(image_df=image_df)

    def make_summary(self, image_df, image_width="100px"):
        out_df = image_df[
            image_df.columns.difference(list(self.name_folder_map))
        ].copy()
        for col, out_dir in self.name_folder_map.items():
            out_df[col] = None
            for ii, row in image_df.iterrows():
                out_df.at[ii, col] = f"<img src='{out_dir / f'{ii}.png'}' />"

        image_style = f"<style type='text/css'>img {{width:{image_width}}}</style>"
        return "\n".join([image_style, out_df.to_html(escape=False)])


class FeatureExtractorWorkflow(WorkflowDirectory):
    region_properties_file = RegisteredPath(".csv")

    @classmethod
    def get_feature_list(cls, include_bbox=False):
        prop_names = list(regionprops(np.ones((5, 5), int))[0])
        # May or may not exist depending on scikit version
        unused = {
            # fmt: off
            "convex_image",    "image_convex", "coords",
            "filled_image",    "image_filled", "intensity_image",
            "image_intensity", "image",        "centroid",
            "label",           "moments",      "slice"
            # fmt: on
        }
        if not include_bbox:
            unused.add("bbox")
        for prop in unused.intersection(prop_names):
            prop_names.remove(prop)
        return prop_names

    @staticmethod
    def eft_features(mask):
        contour = ComplexXYVertices.fromBinaryMask(mask).stack().view(np.ndarray)
        return elliptic_fourier_descriptors(contour, normalize=True)

    def runWorkflow(self, image_df, save_to_disk=True):
        prop_names = self.get_feature_list()
        all_props = []
        for cur_index, mask in image_df["thresholded"].items():
            labeled_mask = label(mask)
            out_dict = regionprops_table(
                labeled_mask,
                properties=prop_names,
                extra_properties=[self.eft_features],
            )
            out_dict["index"] = cur_index
            subdf = pd.DataFrame(out_dict)
            all_props.append(subdf)
        props_df = pd.concat(all_props).set_index("index", drop=True)
        if save_to_disk:
            props_df.to_csv(self.region_properties_file)
        return dict(region_properties_df=props_df)


class FeatureSelectionWorkflow(WorkflowDirectory):
    transformed_features_file = RegisteredPath(".csv")
    untransformed_features_file = RegisteredPath(".csv")
    ranked_properties_file = RegisteredPath(".csv")

    def runWorkflow(self, region_properties_df: pd.DataFrame, save_to_disk=True):
        # Drop ill-defined features + image column to stage for numeric analysis
        # First, take out columns that are all-null
        numeric_props_df = region_properties_df.dropna(axis=1, how="all")
        # Now, take out rows that are have null values since they screw with analysis
        numeric_props_df = numeric_props_df.dropna(axis=0, how="any")

        scaler = StandardScaler()
        numeric_props_df = pd.DataFrame(
            scaler.fit_transform(numeric_props_df), columns=numeric_props_df.columns
        ).set_index(numeric_props_df.index)

        # PCA on features to retain the most informative ones
        pca = PCA(n_components=0.99)
        pca_features = pca.fit_transform(numeric_props_df)

        # What original features contributed the most variance?
        pca_importance_df = pd.DataFrame(
            pca.components_.T,
            index=numeric_props_df.columns,
            columns=[f"PC{i}" for i in range(pca.n_components_)],
        )
        weighted_variances = pca_importance_df * pca.explained_variance_ratio_.reshape(
            1, -1
        )
        sorted_features = weighted_variances.sum(axis="columns").sort_values(
            ascending=False, key=abs
        )
        ranked_properties_ser = sorted_features / sorted_features.abs().sum()
        pca_features = pd.DataFrame(
            pca_features,
            columns=[f"PC {ii}" for ii in range(pca_features.shape[1])],
            index=numeric_props_df.index,
        )
        if save_to_disk:
            pca_features.to_csv(self.transformed_features_file)
            numeric_props_df.to_csv(self.untransformed_features_file)
            ranked_properties_ser.to_csv(self.ranked_properties_file)
        return dict(
            transformed_features_df=pca_features,
            untransformed_features_df=numeric_props_df,
            ranked_properties=ranked_properties_ser,
        )


class HistogramExtractorWorkflow(WorkflowDirectory):
    histogram_data_file = RegisteredPath(".csv")
    histogram_scores_file = RegisteredPath(".csv")
    bin_edges_file = RegisteredPath(".csv")

    def runWorkflow(
        self,
        annotations_df,
        image_df,
        # transformed_features_df,
        untransformed_features_df,
        save_to_disk=True,
        plot=False,
    ):
        image_files = annotations_df.loc[
            untransformed_features_df.index, RTF.IMAGE_FILE
        ]
        hist_df, edges = self._do_histogram_separation(
            untransformed_features_df, image_files
        )
        scores_df = self.get_correlation_scores(hist_df)
        if save_to_disk:
            pd.Series(edges).to_csv(self.bin_edges_file)
            hist_df.to_csv(self.histogram_data_file)
            scores_df.to_csv(self.histogram_scores_file)
        if plot:
            self.plot_histograms(hist_df, edges)
        return dict(histogram_df=hist_df, bin_edges=edges)

    @staticmethod
    def _do_histogram_separation(
        analysis_df, image_file_series, boards: list[str] = None
    ):
        if boards is None:
            boards = image_file_series
        boards = list(set(boards))
        grouped = [
            analysis_df[(image_file_series == board).to_numpy(bool)] for board in boards
        ]
        bin_edges = np.linspace(-3, 3, 64)
        entries = []
        for feature in analysis_df:
            full_histogram, _ = np.histogram(analysis_df[feature], bins=bin_edges)
            normed_full_hist = full_histogram / full_histogram.sum()
            all_boards_entry = dict(board="<all>", feature=feature)
            for ii in range(len(bin_edges) - 1):
                all_boards_entry[f"hist-{ii}"] = normed_full_hist[ii]
            entries.append(all_boards_entry)
            for board_name, board_df in zip(boards, grouped):
                entry = dict(feature=feature, board=board_name)
                board_hist, _ = np.histogram(board_df[feature], bins=bin_edges)
                # Find worst matches between full and partial distributions
                normed_board_hist = board_hist / board_hist.sum()
                for ii in range(len(bin_edges) - 1):
                    entry[f"hist-{ii}"] = normed_board_hist[ii]
                entries.append(entry)

        histogram_df = pd.DataFrame(entries)
        return histogram_df, bin_edges

    def get_correlation_scores(self, histogram_df: pd.DataFrame):
        scores = []
        boards = histogram_df["board"].unique()
        for feature, group_df in histogram_df.set_index("board").groupby("feature"):
            for ii, board_a in enumerate(boards):
                for board_b in boards[ii + 1 :]:
                    hists = [
                        group_df.loc[board].filter(like="hist-")
                        for board in [board_a, board_b]
                    ]
                    score = np.correlate(*hists, mode="same").max()
                    scores.append(
                        dict(
                            feature=feature,
                            board_a=board_a,
                            board_b=board_b,
                            score=score,
                        )
                    )
                    scores.append(
                        dict(
                            feature=feature,
                            board_a=board_b,
                            board_b=board_a,
                            score=score,
                        )
                    )
        return pd.DataFrame(scores).sort_values(by=["feature", "board_a", "score"])

    @staticmethod
    def plot_histograms(
        histogram_df, bin_edges: np.ndarray, features=None, n_subplots=5
    ):
        if features is None:
            features = np.random.choice(histogram_df["feature"].unique(), n_subplots)
        else:
            n_subplots = len(features)
        boards = np.array(list(set(histogram_df["board"]) - {"<all>"}))
        histogram_df = histogram_df.set_index(["feature", "board"]).filter(like="hist-")

        bin_width = bin_edges[1] - bin_edges[0]
        fig, axs = plt.subplots(2, n_subplots)
        for ii in range(n_subplots):
            cur_feature = features[ii]
            axs[0, ii].set_title(cur_feature)
            axs[0, ii].bar(
                bin_edges[1:], histogram_df.loc[(cur_feature, "<all>")], width=bin_width
            )
            for board in boards:
                hist_values = histogram_df.loc[(cur_feature, board)]
                axs[1, ii].bar(bin_edges[1:], hist_values, alpha=0.6, width=bin_width)
        plt.show(block=True)
        return fig

    @staticmethod
    def get_most_separable_features(self, scores_df, n_top=10):
        cmp_against_all = scores_df.query("board_a == '<all>'")
        feature_scores = (
            cmp_against_all.groupby("feature")["score"]
            .quantile(0.1)
            # Lower scores mean higher separability (therefore ascending=True)
            .sort_values(ascending=True)
        )
        return feature_scores.index[:n_top]

main_wf = MainWorkflow("./workspace", stages=[])
for cls in (
    AnnotationReaderWorkflow,
    ThresholdWorkflow,
    FeatureExtractorWorkflow,
    FeatureSelectionWorkflow,
    HistogramExtractorWorkflow,
):
    stage = cls(folder="")
    main_wf.addStage(stage)

if __name__ == "__main__":
    main_wf.createDirectories()
    out = main_wf.activate()
