from typing import Callable, Sequence

import cv2 as cv
import numpy as np
import pandas as pd
import pyqtgraph as pg
from qtextras import (
    RunOptions,
    MaskCompositor,
    bindInteractorOptions as bind,
)

from s3a import ComplexXYVertices, defaultIo


def iou_loss(ii, poly):
    polyB = np.delete(poly, ii, axis=0)
    maxVerts = ComplexXYVertices([poly, polyB]).stack().max(0)
    maskShape = tuple(maxVerts)[::-1]
    maskA = ComplexXYVertices([poly]).toMask(maskShape=maskShape)
    maskB = ComplexXYVertices([polyB]).toMask(maskShape=maskShape)
    # Vertices are removed when loss is low.
    return 1 - (maskA & maskB).sum() / (maskA | maskB).sum()


def angle_loss(ii, poly):
    # Find adjacent vertices
    considerIdxs = np.arange(ii - 1, ii + 2) % len(poly)
    nbrs = poly[considerIdxs].astype(float)

    # Importance is a measure of vector length and angle between vectors
    # Priority given to large angle and large line segments
    vecs = np.diff(nbrs, axis=0)
    mags = np.prod(np.linalg.norm(vecs, axis=0))
    return (1 - np.dot(*vecs) / mags) * mags


def dummy_loss(ii, poly):
    return ii


def remove_n_points(
    poly,
    n,
    importance_function=angle_loss,
    return_importances=False,
    callback=lambda reduced_poly, importances: None,
):
    cur_n_pts = len(poly)
    importances = np.array([importance_function(ii, poly) for ii in range(cur_n_pts)])
    # Iterate until desired number of vertices is reached
    for ii in range(n):
        min_importance_idx = np.argmin(importances)
        # Remove least important vertex
        poly = np.delete(poly, min_importance_idx, 0)
        importances = np.delete(importances, min_importance_idx)
        cur_n_pts -= 1
        # Recalculate importances around removed point
        min_importance_idx %= cur_n_pts
        if cur_n_pts > 1:
            for neighbor in min_importance_idx, (min_importance_idx - 1) % cur_n_pts:
                importances[neighbor] = importance_function(neighbor, poly)
        callback(poly, importances)
    if return_importances:
        return poly, importances
    return poly


class ComponentDownsampler(MaskCompositor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.downsampled_df = pd.DataFrame(columns=["importance", "mask"])

        self.change_index_proc = self.toolsEditor.registerFunction(
            self.change_index, runOptions=RunOptions.ON_CHANGED
        )

    def set_poly(self, poly: np.ndarray, losses: Sequence[Callable]):
        complex_poly = ComplexXYVertices([poly]).simplify()
        # Grab simplified vertices to take out many redundant points
        poly = complex_poly.stack()
        mask = complex_poly.toMask()
        self.setImage(mask)
        self.clearOverlays()

        mask_shape = tuple(poly.max(0)[::-1])
        loss_names = [loss.__name__ for loss in losses]

        def callback(reduced_poly, _importance, name):
            mask = ComplexXYVertices([reduced_poly]).toMask(mask_shape)
            _importance = np.column_stack([reduced_poly, _importance])
            self.downsampled_df.at[name, "mask"].append(mask * (loss_names.index(name) + 1))
            self.downsampled_df.at[name, "importance"].append(_importance)

        self.downsampled_df = self.downsampled_df.drop(self.downsampled_df.index)
        for downsample_method in losses:
            newRecord = pd.Series(
                data=[[], []],
                index=self.downsampled_df.columns,
                name=downsample_method.__name__,
            )
            self.downsampled_df.loc[newRecord.name] = newRecord
            remove_n_points(
                poly,
                len(poly) - 1,
                downsample_method,
                callback=lambda reduced_poly, _imp: callback(
                    reduced_poly, _imp, downsample_method.__name__
                ),
            )
            mask_value = loss_names.index(downsample_method.__name__) + 1
            self.addLabelMask(mask.copy() * mask_value, name=downsample_method.__name__)
        self.change_index_proc.parameters["idx"].setLimits([0, len(poly) - 2])

    @bind(idx=dict(type="slider"))
    def change_index(self, idx=0):
        for name, row in self.downsampled_df.iterrows():
            self.recordDf.at[name, "item"].setImage(row["mask"][idx], autoLevels=False)


if __name__ == "__main__":
    df = defaultIo.importCsv(
        r"C:\Users\ntjess\Desktop\datasets\mock_fpic\smd_annotation\pcb_8f_cc_4_smd.csv"
    )

    poly = df.at[147, "Vertices"].stack()
    poly -= poly.min(0, keepdims=True)

    pg.mkQApp()
    dsc = ComponentDownsampler()
    dsc.set_poly(poly, [dummy_loss, iou_loss, angle_loss])
    dsc.change_index()
    dsc.showAndExec()
