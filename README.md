## Installation
Everything should work if you install the required packages from `s3a-workflows`:
```bash
# Assuming `s3a-workflows` is cloned from https://gitlab.com/s3a/s3a-workflows
# At time of writing, hash is 82ae014
pip install -r s3a-workflows/requirements-pinned.txt

# silkscore is the only module needing `s3a-workflows` as well
pip install -e s3a-workflows
# Now, install the package
pip install -e smd-techniques
```